# langid
## Update 01
What was done:
* Additional EDA was performed in jupyter notebook (see Update 01 section)
* Grid-search over higher grid of params was performed (see notebook)
* Improved: Preprocessing was improved (+ symbols & abbreviations)
* Improved: Model accuracy is improved using `min_df`, `max_df` params for TfIdfVectorizer()
* Improved: Number of features was reduced from `3219859` to `24779`
* Improved: Model size is decreased from ~90Mb to ~29Mb
* Added: The model now can detect blocks of different languages and group them using __Run-length encoding__ algorithm
* Added: New file `top50_features_by_language.csv` contains Top-50 features for each language
* Added: New folder `./examples/` to test multilingual texts (there is a 'war_and_peace' example)

Author: Oleg Khomenko, Apr. 2018
