import sys
import re  # preprocess data
import os  # Working with file system
import json
import pathlib  # python 3.5+ is required
from itertools import groupby
import pandas as pd  # get labels from language.json
from sklearn.externals import joblib  # load model


# Constants
RANDOM_STATE = 17
LANGUAGES_FILE = './languages.json'
MODEL_FILE = './model.pkl'

# Global Vars
model = False

# supported languages
with open(LANGUAGES_FILE) as json_data:
    languages = json.load(json_data)
    labels = pd.DataFrame(languages).T.sort_values('class').index.values


def preprocess_tweet(s):
    """
    This function preprocess text, removing twitter-specific words 
    @links, #hashtags, http(s)://URLS,
    numbers, newline symbols, special symbols (!$%^&*)
    """
    s = re.sub(
        r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', s, flags=re.MULTILINE)  # remove_urls
    s = re.sub(r'#(\w+)', '', s, flags=re.MULTILINE)  # remove_hashtags
    s = re.sub(r'@(\w+)', '', s, flags=re.MULTILINE)  # remove_at
    # added
    s = re.sub(r'[0-9]+|\n|', '', s)  # numbers and \n symbols
    s = re.sub(r'[A-Z]{2,}', '', s)  # Abbreviations
    s = re.sub(r'[!$%^&*]', '', s)  # Special chars
    return s


def encode(input_string):
    """
    RLA
    We need this one to detect sequences of similar classes and group them into blocks
    """
    return [(len(list(g)), k) for k, g in groupby(input_string)]


def load_model():
    global model
    try:
        print("\nModel is loading... (it may take several minutes)")
        model = joblib.load(MODEL_FILE)
        model.set_params(tfidf__input='content')
        print("Model is successfully loaded..")
    except Exception as e:
        print(e)


def predict():
    """
    Main prediction function
    """
    path = input("\nPlease enter file path\n")
    if not os.path.exists(path):  # check if data was parse before
        print("Wrong Path")
    else:
        print("Start predicting for {}".format(path))
        with open(path, 'r') as f:
            doc = f.read()
        try:
            # List of sentences splitted by 'dot'
            list_of_sentences = doc.split(".")
            # Predictions for each sentence, then merge into groups
            df = pd.DataFrame(encode(model.predict(list_of_sentences)))
            doc_class = model.predict([doc])
            counter = 0  # We use this to iterate over groups and print results
            for i in df.index.values:
                label = df.iloc[i, 1]  # Label for current group
                # Number of sentences in current group (RLA)
                n_sentences = df.iloc[i, 0]
                counter += n_sentences
                print("---") # Restore blocks of text to print
                text = ".".join(
                    list_of_sentences[counter - n_sentences:counter])
                if len(text) > 200: # if block is big -> print only first and last sentences.
                    print(text[:100], "\n...\n", text[-100:])
                else:
                    print(text)
                print("---\nclass: {}".format(labels[label]))  # Class label
            print('\nThe whole document is:\t', doc_class)

        except Exception as e:
            print(e)


def print_menu():
    msg = """Type 'load_model' to load model
Type 'predict' to pass path to the file to be classified
Type 'q' to exit
    """
    print(msg)


if __name__ == '__main__':
    print("--------- Welcome ---------")
    actions = {"load_model": load_model, "predict": predict}
    while True:
        print_menu()
        selection = input("Your selection: ")
        if selection == 'q': # Exit
            break
        action = actions.get(selection, lambda: print("No such action"))
        action()
