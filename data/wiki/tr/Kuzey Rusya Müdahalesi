Kuzey Rusya Müdahalesi, ya da bilinen diğer adlarıyla Kuzey Rusya Seferi, Başmelek Seferi ve Murman Yayılımı, Ekim Devrimi'nden sonra Rus İç Savaşı'na itilaf devletlerinin yaptığı müdahalenin bir parçasıydı. Müdahalede, itilaf devletleri Rus İç Savaşı'nda Beyaz Ordu'ya destek verdi. Hareket mağlubiyetle sonuçlandığı sırada, İtilaf devletleri, Bolshie Ozerki savaşında Bolşeviklere karşı başarılı sonuçlar aldılar ve Bolşeviklerin Rusya'dan iyi bir şekilde çekilmelerine izin verdiler. Müdahale, I. Dünya Savaşı'nın sonlarından (1918), 1920'ye kadar devam etti.


== Müdahalenin arkasındaki nedenler ==

Mart 1917'de, Rusya'da Çar II. Nikolay'ın görevden alınması ve geçici bir demokratik hükümet kurulması sonrasında, ABD hükümeti, I. Dünya Savaşına girdi. İttifak Devletleri Meksika'yı kendi taraflarında savaşa davet etmeye çalıştıktan sonra ABD, Nisan ayında Alman İmparatorluğuna ve daha sonra Avusturya-Macaristan hükümetine savaş ilan etti. Aleksandr Kerenski liderliğindeki Rus Geçici Hükümeti, Alman İmparatorluğuna karşı Doğu Cephesinde savaşmaya devam etme sözü verdi. Buna karşılık ABD, Rus geçici hükümetine ekonomik ve teknik destek sağlamaya başladı, bu şekilde Rusya savaşabilecek gücü elde etti.
18 Haziran 1917'de Rusların yaptığı saldırıya, Alman kuvvetleri karşı atak yaptı. Rus Ordusu ülkedeki isyan ve istifalarla ilgileniyordu. Hâlâ transit geçişte bulunan itilaf devletlerinin hırdavatları, Arhangelsk (Archangel) ve Murmansk'un buzlu limanlarının depolarında birikmeye başladı. Rusya'yı savaşta tutmakla ilgili endişe duyan Kraliyet Donanması, Amiral Kemp liderliğinde İngiliz Kuzey Rusya Filosunu kurdu.
Vladimir Lenin liderliğindeki Bolşevikler, Ekim 1917'de iktidara geldi ve Rusya Sovyet Federatif Sosyalist Cumhuriyeti'ni kurdu. Beş ay sonra, Almanya ile Brest Litovsk Barış Antlaşması'nı imzaladılar ve resmi olarak Doğu Cephesi'ndeki savaş sona erdi. Bu, Alman ordusunun, Batı Cephesine asker göndermeye başlamasına olanak sağladı (henüz, tükenmiş İngiliz ve Fransız orduları, American Expeditionary Force tarafından takviye almamıştı.)
Antlaşmayla beraber Lenin, Çekoslovak Lejyonunun tarafsız kalıp Rusya'yı terk etmesi durumunda, Sibirya'dan güvenli bir şekilde geçip İtilaf devletlerinin Batı Cephesine katılmalarına izin vereceklerine dair söz verdi. Bununla birlikte, lejyonun 50,000 üyesi Trans Sibirya Demiryolunu kullanarak Vladivostok'a gitti, antlaşmanın bozulmadan önce yalnızca yarısı geldi ve Mayıs 1918'de Bolşeviklere karşı mücadeleri devam etti. Ayrıca, Alman birliklerinin Finlandiya'yı Nisan 1918'de işgal etmesi itilaf devletlerini endişelendirmişti, çünkü Murmansk–Sankt-Peterburg demiryolu işgal edilebilirdi. Bununla birlikte stratejik liman Murmansk ve hatta Arkhangelsk şehri bile kaybedilebilirdi.
Bu olaylar sonucunda, İngiliz ve Fransız liderler, İtilaf güçlerinin Kuzey Rusya'da askeri müdahaleye başlamasına karar verdiler. Üç amaçları vardı; Arkhangelsk'daki İtilaf devletlerinin hırdavat stoklarının Alman veya Bolşevik birliklere kaptırılmasını önlemek; Çekoslovak lejyonunu kurtarmak için bir saldırı düzenlemek; ve Bolşevik orduyu Çekoslovak Lejyonu'nun yardımıyla anti komünizmi destekleyen vatandaşları kullanarak yenmek.
İngilizler ve Fransız liderler, birliklerin ciddi derecede zayıf kalması nedeniyle, ABD Başkanı Woodrow Wilson'dan Kuzey Rusya Kampanyası kapsamında Kuzey Rusya'ya ABD birliklerinden askeri ve ekonomik yardım istedi. Temmuz 1918'de, Amerika Birleşik Devletleri Savaş Bakanlığı'nın tavsiyesine rağmen, Wilson, 339. Piyade Alayı'ndan bir askeri birliğinin kampanyaya sınırlı bir katılımını kabul etti. Hızlıca Amerikan Kuzey Rusya Keşif Gücü organize edildi, daha sonra Kutup Ayısı Seferi adını aldı. Aide Memoire uyarınca, Wilson Rusya'daki Amerikan askerlerinin amacını şöyle dile getirdi, "Rus kuvvetleri tarafından daha sonra ihtiyaç duyulacak olan askeri malzemeleri korumak ve kendi savunma örgütlerine Ruslar tarafından kabul edilebilir bir yardım yapmak."


== Ayrıca bakınız ==
Estonya Bağımsızlık Savaşı


== Kaynakça ==


== Dış bağlantılar ==
American Polar Bears, Amerika istila kuvvetleri, Kuzey Rusya
Polar Bear Anıtlar Derneği
Kraliyet Donanması'nın Kuzey Rusya'ya giden bir hastane gemisinde seyahatin günlüğü, Haziran – Ekim 1919
ABD Dış Kuvvetleri Komutanlığı 1900-1993
Rus Bolşevik Donanması 1919 dosyaları
Kuzey Rusya Keşif Gücü 1919, Yeoman of Signals George Smith'ın dergisi ve fotoğrafları, Kraliyet donanması
Kuzey Rusya'da Bolşeviklerin Müdahalesi Mücadele Edilen Amerikan İstilası Tarihi 1918-1919
Kuzey Rusya'nın Tahliyesi, 1919 (1920)
Polar Bear Expedition Dijital Koleksiyonları Bentley Tarihsel Kütüphanesinde yer alır. Günlükler, haritalar, yazışmalar, fotoğraflar, ephemera, basılı materyaller ve bir film dahil olmak üzere 50'den fazla birincil kaynak materyali bulunur.
ABD Ordusu'nun orijinal filmi [1]