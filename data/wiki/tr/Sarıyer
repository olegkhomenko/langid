Sarıyer İstanbul'un Avrupa yakası'nda yer alan ilçedir. Güneyde Beşiktaş, güneybatıda Şişli ve batıda Eyüp ilçeleri ile doğuda İstanbul Boğazı, kuzeyde Karadeniz ile çevrilidir. Sarıyer İlçesi toplam 35 mahalleden oluşmaktadır. Bahçeköy Belediyesi'nin 2009 yılında feshedilerek Sarıyer ilçesinin bir mahallesi olması, 2012 yılında da Şişli sınırları içinde yer alan Ayazağa, Maslak ve Huzur mahallelerinin bu ilçeye dahil edilmesiyle son halini alan ilçe İstanbul'un en kuzeydeki ilçelerinden biridir.


== Etimoloji ==
İlçenin ilk adı Simas'tır. Sarıyer adının kökeni konusunda, kimisi yakıştırma olduğu hemen belli olan farklı anlatımlar vardır. Fatih Sultan Mehmed'in iki sarışın askerinin burada Merkez Camii yanında gömülü oldukları, bu yüzden “Fatih'in sarı erleri”nden bozularak yöreye Sarıyer dendiği veya öteden beri mesire yeri olan yörede Mısırlı zenginlerin harcadıkları altınları yüzünden bölgenin adının “Sarı lira yer”den Sarıyer'e dönüştüğü gibi rivayetler yanında tutarlı görülen varsayım, Sarıyer'in kuzeybatısında Maden Mahallesi'ne doğru sırtların altın madeni ve kil yüzünden sarı renkte olmaları ve buradaki yerleşmeye bu sarı topraklar nedeniyle önce Sarıyar adı verilmiş olmasıdır. Osmanlı kaynaklarında XIX. yüzyıla kadar Sarıyar kullanımı sürmüştür. Yar sözü uçurum anlamının yanı sıra dağ yamacı, dağ sırtı anlamını da taşır. Bir başka rivayet de bölgede doğal yayılım gösteren katırtırnağı bitkisinin çiçeklendiği zaman tüm bölgenin belirgin bir şekilde sarı olmasından aldığı yönündedir.


== Tarih ==

Sarıyer İlçesi sınırları içindeki bazı yerleşme alanlarının Bizans döneminden beri meskun olduğu bilinir. Bunlar özellikle kıyılardaki koylarda bulunan bazı ayazma, kilise, eski liman, sarnıç ve eski kaleler çevresindeki birkaç hanelik küçük kırsal yerleşmelerden oluşuyordu. Buralarda yaşayanlar geçimlerini genellikle balıkçılıktan sağladıklarından bu yerleşimler bir balıkçı köyü niteliği taşıyordu.
Türklerin ilk kez bu köylerden Baltalimanı'na yerleştiği söylenir. İstanbul Boğazı'na dökülen bütün akarsular gibi Baltalimanı Deresi'nin de ağız bölümü eskiden bir haliçti. Bu küçük haliç, boğazda seyreden tekneler için önemli bir doğal barınaktı. Buradaki büyük sarnıcın, yanaşan gemilerin su ihtiyacını karşılamak amacıyla yaptırıldığı sanılır. İstanbul'un fethinden önce Kaptan-ı Derya Baltaoğlu Süleyman Bey'in donanmasını burada korumaya aldığı ileri sürülür. Eski kaynaklarda rastlanmamasına karşın Baltalimanı adının bu olay nedeniyle Baltaoğlu Süleyman Bey'den geldiği söylenir.
Boğaz kıyısındaki küçük köylerin gelişmeye başlaması 16. ve 17. yy'lara rastlar. Bu dönemde Sarıyer, Yeniköy ve Rumeli Hisarı gelişmiş birer köy haline geldi. 18. yy'a gelindiğinde saraya yakın bazı kişilere ait yalılar bu kıyıda belirmeye başladı. Padişah izniyle bazı gayrimüslim ailelerin bu köylere yerleşmeleri de aynı yüzyıla rastlar. 19. yy'ın başlarında şayak ve fes boyama sanatını öğretmeleri için Trakya'dan bazı köylüler İstanbul'a getirildi ve Baltalimanı ile Emirgan arasına yerleştirildi. Bu köylülerin yerleştirildiği Baltalimanı ile Emirgan arasındaki alan Boyacıköy olarak adlandırıldı. İstanbul'daki elçilikler bu yüzyılda yazlık olarak kullanmak üzere kıyı boyunca uzanan geniş arazileri elde ettiler. Bütün bu gelişmelere karşın İstanbul Boğazı kıyısındaki köyler balıkçılıkla uğraşma geleneğini yakın zamana kadar sürdürdü. Bu köylerin iskelelerine bağlı çok sayıda balıkçı teknesinin yanı sıra Boğaz'ın çeşitli yerlerinde dalyanlar vardı.

Cumhuriyet dönemine gelindiğinde, bugünkü Sarıyer ilçe sınırları içindeki yerleşimler, gelişimi donmuş köyler biçimindeydi. Kırsal alandaki köyler Kilyos Nahiyesi'nin sınırları içindeydi ve bu nahiye de Çataca Vilayeti'ne bağlıydı. İlçenin İstanbul Boğazı kıyısındaki kesimi ise Beyoğlu Kazası'nın sınırlarına dahildi. 1926'da Çatalca'nın kaza yapılarak İstanbul Vilayeti'ne bağlanmasından sonra da bu yönetsel durumda herhangi bir değişiklik olmadı. 1930'da yapılan bir yönetsel düzenleme sonucunda bugünkü Sarıyer İlçesi kuruldu. İlçe merkezinde Sarıyer Belediyesi'nin kuruluşu da aynı tarihe rastlar. 1936'da Kemerburgaz nahiyesinin o yıl kurulan Eyüp ilçesine, 1954'te Maslak ve Ayazağa köylerinin de o yıl kurulan Şişli ilçesine verilmesiyle ilçe sınırları daralmıştır.
1960'lara değin ilçenin Boğaz kıyısındaki semtleri, daha çok yazın kalabalıklaşan sayfiye yeri niteliği taşıyordu. Her semtte bir iskele vardı ve ulaşımda daha çok 19. yy'dan beri yararlanılan suyolu kullanılıyordu. Zaman içinde vapur seferlerinin sıklaşması kıyı semtlerinin gelişmesinde önemli bir etkendi. Bu semtlerin bazılarında kıyı boyunca gazinolar ve yazın büyük ilgi gören plajlar inşa edildi. Ayrıca Belgrad Ormanı'ndaki su başları ve bentler yıl boyunca İstanbulluların ilgisini çeken mesire yerleriydi. Daha sonra hem Maslak üzerinden gelen Büyükdere Caddesi'nin yapılması, hem kıyıyı izleyen sahil yolu'nun genişletilmesi sonucunda karayolu ulaşımı gelişti.
Böylece semtlerinin doğrudan birbirine bağlanması sonucunda bu ulaşım eksenleri boyunca mevcut semtlerin gelişmesi ve bu semtler arasındaki boş alanların yerleşime açılması süreci başladı. Kıyı kesiminde daha çok üst gelir gruplarına ait konutlar ve köşkler, sırt biçiminde uzanan yüksek alanın yamaçlarında da gecekondu mahalleleri ortaya çıktı. İstanbul Boğazı'na bakan yamaçlarda oluşan bu yapılaşmalar bir yandan ilçenin doğal yapısını tahrip ederken öbür yandan da yörenin görünümünü önemli ölçüde çirkinleştirdi. 1980'lerin sonundan itibaren köylerin, hem İstanbul şehir merkezine yakın olmak isteyen hem de doğaya yakın olmak isteyen üst gelir grubunun yerleşmeye başlaması sonunda çehresi önemli ölçüde değişmiştir.


== Coğrafya ==

İlçe toprakları, Çatalca Yarımadası'nın en doğu kesiminde yer alan sırtın, bir yandan İstanbul Boğazı'na, öbür yandan da kuzeyde Karadeniz'e doğru alçalan bölümlerden oluşur. Kuzey-güney doğrultusunda uzanan bu sırtın batı yamaçlarından çıkan sular (Göksu Deresi, Şeytandere ve Ayazağa Suyu) Kağıthane Deresi aracılığıyla Haliç'e, kuzey kesiminden doğan sular Karadeniz'e, doğu yamaçlarından kaynaklanan sular da İstanbul Boğazı'na ulaşır. Günümüzde bunlardan en önemlileri İstanbul Boğazı'na doğru akan Sarıyer, Bakla ve Baltalimanı dereleridir.
Sarıyer İlçesi'nin Karadeniz kıyısı yer yer düz ve kumsal, bazı kesimlerde de falezlidir. Batıda, Kısırkaya'dan Kilyos'a (Kumköy) kadar uzanan kıyıdaki kumsal, doğuda Kilyos ile Rumelifeneri arasında, yerini kayalık falezlere bırakır. İstanbul Boğazı girişindeki Rumelifeneri açıklarında yer alan kayalıklara Öreke Adaları denir. İlçenin İstanbul Boğazı kıyıları oldukça girintili çıkıntılıdır. Bu kıyıdaki en önemli girinti Çayırbaşı'na doğru bir körfez gibi sokulan Büyükdere Koyu, başlıca çıkıntı ise doğuya doğru bir burun oluşturan Yeniköy'dür. Boğaz kıyısında yer alan başlıca küçük ve dar girintiler ise Tarabya ve İstinye koylarıdır.
Sarıyer, doğal bitki örtüsü açısından İstanbul'nin zengin ilçelerinden biridir. Belgrad Ormanı'nın doğu ucu ilçe sınırları içine sokulur. Ayrıca Rumelikavağı-Rumelifeneri-Kilyos üçgeni içinde kalan alan, büyük ölçüde ormanlarla kaplıdır. Eskiden bu alanda çok daha sık olan ormanlar, varlıklı İstanbullular için ikinci konut yapımına girişen kooperatifler tarafından yer yer tahrip edilmiştir.


== Yönetim ==
Bugünkü ilçe toprakları 1930'a kadar Beyoğlu Kazasıyla Çatalca Vilayeti sınırları dahilindeydi. 1930'da yapılan bir yönetsel düzenleme sonucunda bugünkü Sarıyer ilçesi kuruldu. İlçe merkezinde Sarıyer Belediyesi'nin kuruluşu da aynı tarihte olmuştur. 1990'larda belediye olan Bahçeköy Beldesi 2008 yılında fesh olarak Sarıyer ilçesinin mahallesi oldu. 2012 yılında da Şişli sınırları içinde yer alan Ayazağa, Maslak ve Huzur mahalleleri bu ilçeye dahil edildi. Yine aynı düzenlemeyle 8 köyün mahalle statüsüne geçmesiyle ilçenin mahalle sayısı 38'e yükselmiştir.


== Turizm ==

Sarıyer, doğal ve tarihsel değerler açısından zengin bir ilçedir. Orman varlığının yanı sıra su kaynakları da eskiden beri İstanbul için önem taşımıştır. Bu su kaynaklarından en ünlüleri Çırçır, Hünkar, Kocataş ve Sultan sularıdır. İlçe arazisindeki akarsular küçük olmalarına bakılmadan çok eskiden beri İstanbul'un su ihtiyacını karşılamak için değerlendirilmiştir. Bu amaçla dereler üzerinde bazı bentler ve bentlerin ardında biriken suları kente akıtmak için sukemerleri inşa edilmiştir. Bunlardan II. Mahmut, Topuzlu, Kirazlı ve Valide bentleri ile Bahçeköy ve II. Mahmud sukemerleri ilçe sınırları içindedir. İstanbul'un akciğerlerini oluşturan alanlardan biri olan Belgrad Ormanı'nda yer alan gezi yolları ve piknik alanları özellikle hafta sonlarında halkın ilgisini çeker. Belgrad Ormanı içindeki İstanbul Üniversitesi Orman Fakültesi'ne bağlı Atatürk Arboretumu bilimsel araştırmalara her yönü ile açık bir canlı laboratuvar olarak hizmet vermektir.
İlçe, eskiden kıyılarındaki plajlarıyla da ünlüydü. Ama günümüzde İstanbul Boğazı kıyısında yalnızca Altınkum ve Tarabya plajları faaliyet halindedir. Deniz kirliliğinin yoğunlaşması nedeniyle, dalyanlar gibi Boğaz'daki öbür plajlar da kapanmıştır. İlçenin en gözde plajı Kilyos'tadır. Sarıyer'ın, özellikle Karadeniz kıyısındaki plajları, yaz aylarında bir turistik cazibe merkezi halini alır. Özellikle kıyı kesiminde otel, motel, lokanta ve bar gibi işyerleri yıl boyunca İstanbulluların ilgisini çeker.
İlçede mimari değer açısından önem taşıyan birçok yapı vardır. Bunlar arasında konsolosluk binaları, kasırlar, köşkler, yalılar, iskeleler, kiliseler dikkati çeker. Ayrıca Rumelifeneri'ndeki Ceneviz kalesi de görülmeye değer yerlerindendir.


=== Önemli ve tarihi mekanlar; ===


== Galeri ==


== Video Galeri ==


== Kamu hizmetleri ==


=== Eğitim ===

İstanbul Teknik Üniversitesi, Marmara Üniversitesi, Boğaziçi Üniversitesi, Koç Üniversitesi, Işık Üniversitesi, Beykent Üniversitesi ve İstanbul Üniversitesi Orman Fakültesi, Türk Silahlı Kuvvetleri Harp Akademileri ile Adile Sadullah Mermerci Polis Meslek Yüksekokulu Sarıyer İlçesi'nde bulunan yüksek öğretim kurumlarıdır. 2009 yılı itibarıyla Sarıyer İlçesi sınırları içinde 47 okulöncesi, 50 ilköğretim ile 31 lise ve dengi eğitim kurumu vardır.


=== Sağlık ===
İlçe sınırları içinde 5 hastane (Metin Sabancı Baltalimanı Kemik Hastalıkları Eğitim ve Araştırma Hastanesi, İstinye Devlet Hastanesi, İsmail Akgün Devlet Hastanesi, Özel Acıbadem Maslak Hastanesi, Özel Acıbadem Zekeriyaköy Hastanesi), 23 sağlık ocağı, 99 eczane ve 7 poliklinik bulunur.


=== Ulaşım ===

Sarıyer İlçesi'nde yaşayanlar deniz ve kara yollarından yararlanarak kentin öbür kesimlerine ulaşırlar. Denizyolu ulaşımı semtlerdeki iskelelerden (Sarıyer, Rumelikavağı, Yeniköy, Emirgan, İstinye, Büyükdere) İDO'nun vapurlarıyla sağlanır. İlçe kıyılarında balıkçı teknelerinin sığındığı bazı iskeleler vardır. Bunlardan başlıcaları Rumelifeneri'ndeki balıkçı barınağı ile Sarıyer'deki küçük dalgakırandır.
Kentin batı kesimini İstanbul Boğazı yakınından Karadeniz kıyısına bağlayan karayolları Sarıyer İlçesi'nde sona erer. İlçede üç önemli karayolu ekseni vardır. Bunlardan biri Boğaz kıyısını izleyen ve değişik semtlerde farklı adlar taşıyan “sahil yolu”, ikincisi ise Zincirlikuyu'dan gelip Tarabya kavşağından sonra Hacı Osman Bayırı adıyla anılan ve Kefeliköy'de sahil yoluna bağlanan Büyükdere Caddesi'dir. Büyükdere Caddesi, İstinye'den ve Tarabya'dan da sahil yoluna bağlanmaktadır. Üçüncü önemli karayolu ekseni, batı ayağı Rumelihisarı'nda olan Fatih Sultan Mehmet Köprüsü'yle Avrupa Yakası'nı Anadolu Yakası'na bağlayan batı-doğu doğrultulu O-2 Otoyolu'dur. Sarıyer İlçesi'ne, İstanbul'un geneline olan kara ulaşımının yalnızca bu iki aks üzerinden olması nedeniyle “çıkmaz sokak” da denir. İlçe merkezinden Beşiktaş ve Taksim'e otobüs, 4. Levent ve Beşiktaş'a da minibüs seferleri vardır.
Ayrıca ilçe halkı şu an için M2 Metro Hattının en kuzeydeki dört istasyonu olan Hacıosman, Darüşşafaka, Atatürk Oto Sanayi ve İTÜ Ayazağa istasyonlarından metroya binerek Levent, Mecidiyeköy, Taksim güzergahından Yenikapı durağına kadar ulaşabilmektedirler.


== Spor ==

İlçenin en ünlü takımı olan, adını ilçeden alan futbol, voleybol ve boks dallarında faaliyet gösteren Sarıyer Spor Kulübü 1940 yılında kurulmuştur. 1982-1994 ve 1996-1997 yılları arasında Süper Lig'de oynamış olan Beyaz Martılar, boğazın en büyük takımı ve bir zamanlar üç büyüklerin korkulu rüyası olmuştur. Kulübün renkleri lacivert ve beyazdır; Lacivertin asaleti, beyazın ise temizliği simgelediği kabul edilmiştir. 1980'ler ve 1990'ların ilk yarısında altın dönemini yaşayan Sarıyer 2009-2010 sezonunda 2. Lig'de mücadele etmektedir. İç saha maçlarını Yusuf Ziya Öniş Stadı oynar.
İlçenin diğer ünlü takımıysa 2003-2005 arasında 3. Lig'de (4. Kademe) oynamış olan Yeniköyspor'dur. Ayrıca Sarıyer Belediyespor, Ferahevlerspor, Bahçeköyspor Pınar İY., Pınarspor, İstinye, Reşitpaşa, Kireçburnu, Madenspor, Rumelikavağı, Çayırbaşı, Büyükdere, Çayırbaşı Özkan gibi takımlar amatör futbol liglerinde mücadelelerine devam etmektedirler.
Galatasaray (futbol takımı)'nın iç saha maçlarını oynadığı Türk Telekom Arena TEM Otoyolu'nun kuzeyinde yer alır. 52.883 seyirci kapasitesi ile Atatürk Olimpiyat Stadyumu'ndan sonra Türkiye'nin ikinci en yüksek kapasiteli stadyumudur. İlçedeki diğer önemli spor tesisleri; Çayırbaşı Stadı, Kilyos Stadı, Mersinli Ahmet Kamp Eğitim Merkezi, Orhan Keçeli Stadı, Sarıyer Kapalı Spor Salonu, Yusuf Ziya Öniş Stadı, Enka Spor Kulübü Sadi Gülçelik Spor Sitesi ile Darüşşafaka basketbol takımının iç saha maçlarını oynadığı Ayhan Şahenk Spor Salonu'dur. İstinye ve Zekeriyaköy'de binicilik tesisleri bulunur.


== Ekonomi ==

Sarıyer İlçesi'nde iktisaden faal olan nüfusu oluşturan kesim, daha çok ilçe dışındaki işyerlerinde çalışır. İlçede fazla sanayi tesisi yoktur. Geçmiş yıllarda kibrit, kablo ve vinç fabrikaları taşınmış, İstinye'deki tersane kaldırılmıştır. Hizmet işkolu ilçenin en canlı ekonomik etkinlik alanını oluşturur.
Özellikle kıyı kesiminde lokanta ve bar gibi işyerleri yılboyunca İstanbulluların ilgisini çeker. Borsa İstanbul İstinye'de bulunmaktadır. Ayrıca ABD, Avusturya, Çin, Irak ve Özbekistan'ın İstanbul başkonsoloslukları Sarıyer İlçesi sınırları içindedir.


== Politika ==


=== Yerel Seçimler ===


=== Genel seçimler ===


== Nüfus ==
2013 ADNKS verilerine göre Sarıyer İlçesi'nin nüfusu 335.598'dir.


== Kaynakça ==
Dünden Bugüne İstanbul Ansiklopedisi, Cilt 6, T.C. Kültür Bakanlığı-Tarih Vakfı (1994)


== Dış bağlantılar ==
Sarıyer Belediyesi
Sarıyer Kaymakamlığı
T.C. İstanbul Valiliği (Harika İstanbul) / Sarıyer İlçesi Tanıtımı