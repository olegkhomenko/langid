Elektriksel elemanların seri bağlanmasında önemli olan elemanlarının birbirine bağlı uçlarının işaretidir. Her bir elemanın (-) ucu sonraki kondansatörün (+) ucuna bağlandığında seri bağlama sağlanmış olur. Yandaki resimde düzgün olarak seri bağlanmış 3 adet kondansatör bulunmaktadır. Seri bağlı elemanların her birinden geçen akım aynıdır. Her bir elemanın uçları arasındaki gerilimin toplamı ise o elemanlara uygulanan toplam gerilimi verir.


== Kondansatör ==
Kondansatörler seri bağlandığı zaman, kaynak akımı her bir kondansatörden geçen akıma eşit olur, kaynak gerilimi ise her bir kondansatörün gerilimlerinin toplamı olur.


=== Zaman domeninde hesap ===

  
    
      
         
        v
        =
        
          v
          
            1
          
        
        +
        
          v
          
            2
          
        
        +
        
          v
          
            3
          
        
      
    
    {\displaystyle \ v=v_{1}+v_{2}+v_{3}}
  

  
    
      
         
        i
        =
        
          i
          
            1
          
        
        =
        
          i
          
            2
          
        
        =
        
          i
          
            3
          
        
      
    
    {\displaystyle \ i=i_{1}=i_{2}=i_{3}}
  

  
    
      
         
        v
        =
        
          v
          
            1
          
        
        +
        
          v
          
            2
          
        
        +
        
          v
          
            3
          
        
        =
        
          
            1
            
              C
              
                1
              
            
          
        
        ⋅
        
          ∫
          
            0
          
          
            t
          
        
        
          i
          
            1
          
        
        d
        t
        +
        
          
            1
            
              C
              
                2
              
            
          
        
        ⋅
        
          ∫
          
            0
          
          
            t
          
        
        
          i
          
            2
          
        
        d
        t
        +
        
          
            1
            
              C
              
                3
              
            
          
        
        ⋅
        
          ∫
          
            0
          
          
            t
          
        
        
          i
          
            3
          
        
        d
        t
      
    
    {\displaystyle \ v=v_{1}+v_{2}+v_{3}={\frac {1}{C_{1}}}\cdot \int _{0}^{t}i_{1}dt+{\frac {1}{C_{2}}}\cdot \int _{0}^{t}i_{2}dt+{\frac {1}{C_{3}}}\cdot \int _{0}^{t}i_{3}dt}
  

  
    
      
         
        v
        =
        (
        
          
            1
            
              C
              
                1
              
            
          
        
        +
        
          
            1
            
              C
              
                2
              
            
          
        
        +
        
          
            1
            
              C
              
                3
              
            
          
        
        )
        ⋅
        
          ∫
          
            0
          
          
            t
          
        
        i
        d
        t
        =
        
          
            1
            
              C
              
                e
                s
              
            
          
        
        
          ∫
          
            0
          
          
            t
          
        
        i
        d
        t
      
    
    {\displaystyle \ v=({\frac {1}{C_{1}}}+{\frac {1}{C_{2}}}+{\frac {1}{C_{3}}})\cdot \int _{0}^{t}idt={\frac {1}{C_{es}}}\int _{0}^{t}idt}
  

Frekans domeninde hesap

  
    
      
         
        V
        =
        
          V
          
            1
          
        
        +
        
          V
          
            2
          
        
        +
        
          V
          
            3
          
        
      
    
    {\displaystyle \ V=V_{1}+V_{2}+V_{3}}
  

  
    
      
         
        I
        =
        
          I
          
            1
          
        
        =
        
          I
          
            2
          
        
        =
        
          I
          
            3
          
        
      
    
    {\displaystyle \ I=I_{1}=I_{2}=I_{3}}
  

  
    
      
         
        V
        =
        
          V
          
            1
          
        
        +
        
          V
          
            2
          
        
        +
        
          V
          
            3
          
        
        =
        
          
            1
            
              j
              ω
              
                C
                
                  1
                
              
            
          
        
        
          I
          
            1
          
        
        +
        
          
            1
            
              j
              ω
              
                C
                
                  2
                
              
            
          
        
        
          I
          
            2
          
        
        +
        
          
            1
            
              j
              ω
              
                C
                
                  3
                
              
            
          
        
        
          I
          
            3
          
        
      
    
    {\displaystyle \ V=V_{1}+V_{2}+V_{3}={\frac {1}{j\omega C_{1}}}I_{1}+{\frac {1}{j\omega C_{2}}}I_{2}+{\frac {1}{j\omega C_{3}}}I_{3}}
  

  
    
      
         
        V
        =
        
          
            1
            
              j
              ω
            
          
        
        (
        
          
            1
            
              C
              
                1
              
            
          
        
        +
        
          
            1
            
              C
              
                2
              
            
          
        
        +
        
          
            1
            
              C
              
                3
              
            
          
        
        )
        I
        =
        
          
            1
            
              j
              ω
            
          
        
        (
        
          
            1
            
              C
              
                e
                s
              
            
          
        
        )
        I
      
    
    {\displaystyle \ V={\frac {1}{j\omega }}({\frac {1}{C_{1}}}+{\frac {1}{C_{2}}}+{\frac {1}{C_{3}}})I={\frac {1}{j\omega }}({\frac {1}{C_{es}}})I}