Arthur Godfrey (31 de agosto de 1903 – 16 de marzo de 1983) fue un locutor, artista y actor radiofónico y televisivo estadounidense, conocido en ocasiones por el apodo de The Old Redhead. Ninguna celebridad televisiva de la década de 1950 disfrutó de la fama de Godfrey, hasta que un incidente ante la cámara minó su imagen e inició su declive.
También fue conocido por su estrecha identificación con muchos de sus patrocinadores, especialmente con los cigarrillos Chesterfield y el té Lipton.[1]​


== Inicios ==
Su nombre completo era Arthur Morton Godfrey, y nació en Nueva York.[2]​ Su madre, Kathryn Morton Godfrey, era de Oswego, Nueva York, y su padre, Arthur Hanbury Godfrey, era inglés. En 1915 la familia se mudó a Hasbrouck Heights, Nueva Jersey.[3]​ Arthur era el mayor de cinco hermanos, y a los 14 años dejó el hogar para buscar trabajo y aliviar la situación económica de la familia. A los 15 era mecanógrafo en la base de Camp Merritt, Nueva Jersey, y dos años después se alistó en la Armada ocultando su verdadera edad.
Godfrey sirvió en la Armada de los Estados Unidos entre 1920 y 1924 como operador de radio en destructores, pero volvió con su familia tras la muerte de su padre. Entre 1927 y 1930 Godfrey sirvió en los Guardacostas de los Estados Unidos, cuerpo en el que tuvo nuevo entrenamiento radiofónico. Durante este período en Baltimore intervino en un programa radiofónico local, consiguiendo suficiente popularidad como para conseguir tener un breve programa semanal.


== Radio ==
Al dejar los Guardacostas, Godfrey fue locutor de la emisora de Baltimore WFBR, trasladándose después a Washington, D.C. para entrar en la emisora propiedad de la NBC WTEM, permaneciendo en la misma hasta 1934. En esa época ya era un ávido aviador, y en 1933 estuvo a punto de morir tras sufrir un accidente de tráfico cerca de Washington, permaneciendo hospitalizado varios meses. Ese período le sirvió para reflexionar sobre el trabajo de los locutores de la época y sobre su conexión con el público, decidiendo que adoptaría un estilo más relajado e informal, como si hablara a una única persona. También adoptó ese estilo para sus comerciales, convirtiéndose en una estrella regional.
Además de comentarista, Godfrey cantaba y tocaba el ukelele. En 1934 se hizo independiente, pero finalmente trabajó en un show diario titulado Sundial en la emisora de la CBS WJSV, en Washington. Godfrey era el disc jockey matutino de la emisora, hacía anuncios comerciales, entrevistaba a invitados y leía noticias.
Por intercesión del Presidente Franklin Delano Roosevelt, que escuchaba su programa, recibió una comisión en la Reserva Naval antes de la Segunda Guerra Mundial. Godfrey finalmente movió su base a la estación de la CBS en Nueva York. En el otoño de 1942 fue comentarista del programa de Fred Allen Texaco Star Theater, en la CBS, pero las diferencias entre ambos motivo su salida del show tras seis semanas de trabajo.
Godfrey se hizo conocido a nivel nacional en abril de 1945 cuando relató en directo la procesión fúnebre del Presidente Roosevelt, con un estilo amable y cordial que, en un momento dado, emocionó a los oyentes. Gracias a ello la CBS le dio un espacio matinal, Arthur Godfrey Time, de cobertura nacional. Emitido cinco días a la semana, sus monólogos y comentarios no tenían guion previo, y se alternaban con entrevistas y momentos musicales. "Arthur Godfrey Time" permaneció en antena hasta 1972.
En 1947 Godfrey consiguió un éxito con la canción humorística "Too Fat Polka (She's Too Fat For Me)", escrita por Ross MacLean y Arthur Richardson. La fama de la canción hizo que The Andrews Sisters grabaran una versión adaptada al punto de vista de las mujeres.
El show matinal de Godfrey se complementó con un programa de variedades, Arthur Godfrey's Talent Scouts. Este programa, una oportunidad para nuevos artistas, era una ligera variación del éxito de la CBS Original Amateur Hour. Entre los intérpretes que intervinieron en Talent Scouts figuraban Lenny Bruce, Don Adams, Tony Bennett, Patsy Cline, Pat Boone, la cantante de ópera Marilyn Horne, Roy Clark, y la vocalista irlandesa Carmel Quinn. Tres destacados descartes para intervenir en el programa fueron Elvis Presley, The Orioles, y The Four Freshmen.


== Televisión ==
En 1948 Arthur Godfrey's Talent Scouts empezó a emitirse simultáneamente por radio y televisión, y en 1952, Arthur Godfrey Time también aparecía en ambos medios. Además, las habilidades de Godfrey como anunciante le facilitaron un gran número de leales patrocinadores, entre ellos el Té Lipton, Frigidaire, Pillsbury Company y los cigarrillos Chesterfield.
En 1949 se inició Arthur Godfrey and His Friends, un programa semanal de variedades. Finalmente, los fines de semana hizo un programa resumen con los mejores momentos de Arthur Godfrey Time, y conocido como Arthur Godfrey Digest. Empezó a evitar las entrevistas a estrellas para favorecer a artistas que participaban de manera regular en el show, y que fueron conocidos como los "Little Godfreys." Muchos de dichos artistas eran desconocidos, pero llegaron a tener fama nacional, como fue el caso de Haleloke, Frank Parker, Marian Marlowe y Julius LaRosa.
Godfrey fue uno de los artistas más ocupados del mundo del espectáculo, a menudo presentando varios programas radiofónicos y televisivos de manera simultánea, incluso más que Robert Q. Lewis, que presentaba Arthur Godfrey Time cuando Godfrey estaba ausente. Ambos hicieron grabaciones comerciales para Columbia Records, entre ellas "Too Fat Polka", "Candy and Cake", "Dance Me Loose", "I'm Looking Over a Four Leaf Clover", "Slap 'Er Down Again, Paw", "Slow Poke" y "The Thing". En 1951 Godfrey fue el narrador de un documental de carácter nostálgico, Fifty Years Before Your Eyes, producido para Warner Brothers por Robert Youngson.
A su manera, Godfrey fue un pionero social. Uno de los componentes de los "Little Godfrey" eran los Mariners, un cuarteto vocal de veteranos guardacostas, tanto blancos como negros. A pesar de algunas quejas de carácter racista, Godfrey se negó a eliminar a los Mariners del reparto de su programa.


== Aviación ==
Godfrey aprendió a volar en los años treinta mientras trabajaba en el área de Washington, D.C., al principio con planeadores y después con aviones. En 1931 sufrió un grave accidente de tráfico que le produjo secuelas que le impidieron servir como piloto durante la Segunda Guerra Mundial, aunque sí fue oficial de la reserva en la Armada.
Su constante complicidad con Eastern Airlines le valió la gratitud de Eddie Rickenbacker, el as de la Primera Guerra Mundial que fue Presidente de la compañía. Tenía tan buena relación con la línea aérea que Rickenbacker tomó un Douglas DC-3 retirado, le preparó un interior ejecutivo y motores de Douglas DC-4, y se lo regaló a Godfrey, que lo utilizaba para viajar a los estudios de Nueva York desde su propiedad en Leesburg, Virginia.
En enero de 1954, Godfrey tuvo problemas con la torre de control del Aeropuerto de Teterboro mientras pilotaba su DC-3. Le suspendieron la licencia durante seis meses. Tuvo un problema similar en Chicago en 1956, aunque no fue sancionado. Estos incidentes, junto a la controversia surgida tras el despido de Julius LaRosa, contribuyeron a debilitar su imagen pública.


== Entre bastidores ==
Tras la cordial imagen del Godfrey presentador existía una personalidad muy controladora. Insistía en que sus "Little Godfreys" siguieran clases de baile y de canto, y en las reuniones con el personal de sus programas era abusivo e intimidante. Esta actitud se hizo más evidente en el caso de LaRosa, cuya fama iba en ascenso.


=== El incidente LaRosa ===
Como muchos hombres de su generación, Julius LaRosa pensaba que las clases de baile eran algo afeminado, y le molestó que Godfrey las ordenara.
Godfrey y LaRosa discutieron cuando LaRosa perdió una lección por motivos familiares. Godfrey le apartó del programa por un día, siendo avisado mediante una nota en un tablero. Tras la sanción, LaRosa contrató a un mánager, y Godfrey consultó inmediatamente con el Presidente de CBS, Frank Stanton, que recordó a Godfrey que LaRosa había sido contratado en directo, y sugería que podía ser despedido del mismo modo.
El 19 de octubre de 1953, al final de su programa matinal, tras presentar a LaRosa y elogiar la interpretación que iba a hacer de la canción "Manhattan," Godfrey anunció que ésta era la última actuación del cantante en el show. Este incidente abrió una etapa de controversia que, paulatinamente, minó la buena imagen de Godfrey.


=== Otros despidos ===
Godfrey despediría a otros de sus regulares, incluyendo al líder de banda Archie Bleyer, al poco tiempo del despido de LaRosa." Bleyer había formado su propio sello, Cadence Records, en el cual grababa LaRosa. Bleyer se casó con una de The Chordettes, y ese grupo también rompió con Godfrey (Godfrey las reemplazó con las The McGuire Sisters).
Un número significativo de otros "Little Godfreys," incluyendo a los Mariners y a Haleloke, fueron despedidos entre 1953 y 1959 sin razón aparente.
Además, Godfrey tuvo problemas con los medios de comunicación y disputas públicas con periodistas como Jack O'Brian. Además empezaron a circular artículos que relacionaban a Godfrey con algunas de las artistas de los "Little Godfreys."
En esa época se estrenaron dos filmes inspirados en la cada vez más controvertida carrera de Godfrey. Se trataba de The Great Man (1956), protagonizada, dirigida y producida por José Ferrer, y el clásico de Elia Kazan A Face in the Crowd (1957), interpretado por Andy Griffith y Patricia Neal.
El programa de Godfrey Talent Scouts se mantuvo en antena hasta 1958.


== Últimos años ==
En 1959 a Godfrey le diagnosticaron un cáncer de pulmón. Tras esta noticia, ese mismo año finalizó Arthur Godfrey Time y The Arthur Godfrey Show.
A pesar de que el cáncer había afectado a la aorta, y de que fue preciso extirparle un pulmón, Godfrey mejoró lo suficiente como para hacer un especial sobre Arthur Godfrey Time en la radio. El show continuó con un formato en el cual participaban invitados como el pianista Max Morath y el vocalista Carmel Quinn. Sin embargo, la audiencia disminuía, por lo que Godfrey y la CBS acordaron finalizar el programa en abril de 1972. En esa época Godfrey era coronel del Mando de Reserva de la Fuerza Aérea y todavía pilotaba.
Godfrey hizo tres películas: Cuatro tíos de Texas (1963), The Glass Bottom Boat (1966), y Where Angels Go, Trouble Follows (1968). Por breve tiempo presentó Candid Camera con su creador, Allen Funt, pero la relación, como tantas otras, acabó mal. Godfrey trabajó en varias ocasiones como artista invitado, y presentó junto a Lucille Ball el especial de la CBS 50 Years of Television (1978). También hizo un cameo en el film de 1979 de serie B Angel's Revenge.
Ya en su retiro, Godfrey fue un eficaz portavoz del movimiento ecologista. Incluso renunció a un lucrativo contrato comercial al constatar que en la fabricación del producto, una pasta dental, se producían componentes químicos implicados en la contaminación del agua.


== Vida personal ==
Godfrey se casó dos veces. Tuvo un hijo con su primera esposa, Catherine, y en 1938 se casó con Mary Bourke, de la cual se divorció en 1983, poco antes de fallecer. Tuvieron tres hijos.
Arthur Godfrey falleció a causa de un enfisema, secunadrio a los tratamientos oncológicos recibidos, en Nueva York en 1983.[2]​[4]​ Godfrey fue enterrado en el Cementerio Unión de Leesburg, Virginia, no lejos de su granja en Waterford, Virginia.


== Galardones ==
Premio NBAA Meritorious Service to Aviation (1950)
National Association of Broadcasters Hall of Fame (radio)
National Aviation Hall of Fame (1987)
Radio Hall of Fame (1988)
Premio Peabody (1971)
Una estrella en el Paseo de la Fama de Hollywood, en el 1551 de Vine Street, por su carrera televisiva.


== Referencias ==


== Enlaces externos ==
Esta obra contiene una traducción derivada de Arthur Godfrey de Wikipedia en inglés, publicada por sus editores bajo la Licencia de documentación libre de GNU y la Licencia Creative Commons Atribución-CompartirIgual 3.0 Unported.
Arthur Godfrey en Internet Movie Database (en inglés)
Arthur Godfrey en Internet Broadway Database (en inglés)
Museum of Broadcast Communication: Arthur Godfrey
Kinescope of an episode of 'Arthur Godfrey Time' at the Internet Archive