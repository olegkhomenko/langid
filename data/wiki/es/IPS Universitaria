La IPS Universitaria es una Institución Prestadora de Servicios de Salud de la Universidad de Antioquia, su sede principal se localiza en el Área de la Salud en la ciudad de Medellín, Colombia. En sí, se encarga en comercializar y prestar servicios de salud principalmente a la comunidad universitaria y de igual manera a la población general. Opera como una corporación mixta sin ánimo de lucro, conformada por la Universidad de Antioquia y por la Fundación de Apoyo a la Universidad de Antioquia, en sus actos administrativos se rige por el derecho privado.


== Reseña histórica ==

La IPS Universitaria fue aprobada por el Consejo Superior de la universidad con la Resolución Superior 632 del 18 de agosto de 1998, conformada por la Universidad de Antioquia y por la Fundación de Apoyo a la Universidad de Antioquia. Inició operaciones el 1 de noviembre del mismo año y la Dirección Seccional de Salud de Antioquia mediante Resolución 1.566 del 4 de diciembre de 1998 le otorgó personería jurídica y en consecuencia cuenta con autonomía administrativa, técnica y financiera.
En el 2006, el Instituto de Seguros Sociales -ISS- por su grave crisis financiera y administrativa entra en liquidación, por lo cual se ponen en venta sus centros hospitalarios. En el 2007, la Universidad de Antioquia junto con la Gobernación de Antioquia compran las clínicas del Seguro Social que incluyen las clínicas León XIII, en Medellín; Víctor Cárdenas Jaramillo, en Bello, y Santa María del Rosario, en Itagüí. Inmediatamente, la IPS Universitaria se encarga de la administración, mejorando considerablemente los servicios médicos, los cuales se encontraban muy mal estado.


== Sedes ==
La IPS Universitaria cuenta con varias sedes esparcidas principalmente en los campus de la Universidad, permitiendo así atender con facilidad a toda la comunidad universitaria.
Sede Ambulatoria: Antes denominada Sede Central, es una instalación ubicada en la carrera 51A Nº 62-42, centro de la ciudad de Medellín, cerca del Hospital Universitario San Vicente de Paúl y de la Facultad de Medicina de la Universidad de Antioquia. Cuenta con 44 consultorios y un personal humano altamente capacitado, esta sede presta servicios certificados de: cirugía ambulatoria, consulta externa, salud oral, biobanco, servicio farmacéutico y de óptica, ayudas diagnósticas y complementación terapéutica de primero y segundo nivel. Además, la Sede Ambulatoria ofrece un amplio portafolio de servicios que está a disposición de varias empresas promotoras de salud (EPS) tanto del régimen contributivo como subsidiado que tienen contratos y convenios vigentes con la Institución.
Sede Universitaria: ubicada en el primer piso del Bloque 22 de la Ciudad Universitaria y funciona desde el 1 de noviembre de 1998. La Sede está dotada de personal conformado por 11 médicos, 1 auditor médico, 1 enfermera, además de quienes hacen parte del laboratorio clínico. Dicha Sede ofrece sus servicios sólo a estudiantes -que no se encuentran afiliados a seguridad social- y a los empleados de la Universidad de Antioquia.
Sede SIU: se localiza en el primer piso del edificio “Sede de Investigación Universitaria” -SIU-, cuenta con 8 consultorios, para atender a los usuarios remitidos por las EPS o a los particulares, con modernas instalaciones, brinda un servicio ágil y con toda la tecnología. En esta sede funciona el Laboratorio del sueño, donde se estudian pacientes con problemas del sueño y se realizan electroencefalogramas. En esta sede es donde se ofrece el servicio de consulta de inmunodeficiencias primarias.
Sede Clínica León XIII: ahora administrada por la IPS Universitaria de la Universidad de Antioquia, es una de las más reconocidas en Medellín, fue inaugurada en 1950. Esta sede presta servicios certificados de: Ayudas diagnósticas, cirugía, consulta externa, hospitalización, laboratorio clínico, UCE neonatal, UCI adultos, UCI neonatal y urgencias. Cuenta con el único Servicio de Alergología Clínica de Colombia con carácter formativo universitario en el postgrado médico de Alergología Clínica, que tiene la Universidad de Antioquia.


== Véase también ==
Universidad de Antioquia
Hospital Universitario San Vicente de Paúl
Ciudad Universitaria de Medellín
Área de la Salud


== Enlaces externos ==
Página oficial Universidad de Antioquia
Página oficial IPS Universitaria