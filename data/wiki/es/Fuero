Los fueros locales, fueros municipales o fueros eran los estatutos jurídicos aplicables en una determinada localidad, cuya finalidad era, en general, regular la vida local, estableciendo un conjunto de norma jurídica/normas, derechos y privilegios, otorgados por el rey, el señor de la tierra o el propio concejo, es decir, las leyes propias de un lugar. Fue un sistema de derecho local utilizado en la Península Ibérica a partir de la Edad Media y constituyó la fuente más importante del Derecho altomedieval español. También fue usado en ciertas zonas de Francia.


== Antecedentes ==

La conquista musulmana de la península ibérica supuso, en el plano jurídico, la ruptura de la unidad que, mediante el Liber Iudiciorum, se había conseguido en el reino visigodo, sin perjuicio de la eventual práctica de algunas costumbres diversas a las señaladas en dicho texto legal.
Frente a esta situación, se respondió jurídicamente de distinto modo, según las circunstancias que se dieron en cada zona del territorio.
El inicio de la reconquista del territorio peninsular dio lugar a la formación de diversos reinos cristianos y la formulación en ellos de un nuevo Derecho, plural y diverso, caracterizado por tratarse, en general, de un derecho esencialmente local.
La empresa de la reconquista no significaba sólo derrotar militarmente a los musulmanes, sino repoblar las zonas conquistadas. En aquellas áreas que, por su valor económico o estratégico, interesaba repoblar, los reyes cristianos y señores laicos y eclesiásticos de la Península Ibérica comenzaron a otorgar una serie de privilegios con el fin de atraer pobladores para que se asentaran allí, como modo de asegurar fundamentalmente las zonas fronterizas y revitalizarlas económicamente. Los documentos en que constaban tales privilegios y exenciones se denominaron cartas pueblas o también llamadas cartas de población (chartae populationis).
Los otorgantes de las cartas pueblas eran los respectivos señores del territorio –rey cristiano o señor laico o eclesiástico–, que actuaban por propia iniciativa (o como delegados del rey) o, en ocasiones, a solicitud de los propios súbditos. En este último caso, les daba a estos acuerdos un cierto carácter de pacto.
Las cartas más antiguas, que se conservaran, datan del siglo IX; siendo concedidas hasta mediados del siglo XII.
A partir de finales del siglo X, el derecho local comenzó a fijarse por escrito, recogiéndose normas de diversas procedencias, atribuyéndose por lo general al otorgante de la primera carta de población. Este proceso derivó en nuevas cartas que poseían la forma de privilegios reales y que se presentaban bajo una diversa nomenclatura –chartae fori, chartae libertatis, confirmationis, privilegii, entre otras–; éstas se han denominado por los investigadores como fueros breves, por su extensión limitada al diploma que los contenía.


== Contenido ==
Los fueros recogían las costumbres de cada localidad, además de los privilegios otorgados por los reyes a las mismas, así como el conjunto de disposiciones que preservaban la nobleza, el clero y el vasallaje de una zona.
Era un pacto solemne entre los pobladores y el rey, y también —por extensión— eran las leyes que regían determinada comarca o localidad.
En un comienzo las pretensiones de los pobladores era la de incluir en el pacto derechos de carácter público. El Derecho privado primeramente estuvo casi excluido. Luego fue progresivamente incorporado en la legislación foral. La razón se debía a que aquellos derechos que estaban en discusión no eran éstos, sino los relacionados con reivindicaciones que los pobladores anhelaban; con su status jurídico. Para la constitución del referido pacto era siempre necesaria la firma real, porque por más que se hubiesen tratado tales reivindicaciones con un noble de rango inferior, era el rey quien juraba respetar y hacer cumplir esos derechos reclamados.
Los fueros como Cartas Pueblas son el conjunto de leyes y libertades entregados a los repobladores de una villa, es decir, una población sin señorío o cuyo señorío correspondía al rey. En estas leyes se detallan las libertades, como la elección de alcalde, tributos a la corona, la obligación de prestar auxilio a la mesnada real con peones y caballeros villanos, y muchas prerrogativas que hacían al hombre de la ciudad más libre que el campesino de régimen feudal (aunque el feudalismo en España es mínimo a excepción de Cataluña y muy limitado en León donde se crean estas legislaciones para hombres libres). A cada fuero le correspondía, aparte de la ciudad o villa, un alfoz o territorio, que contaba con varias aldeas y municipios, dependientes de la villa principal. La población tenía un concejo, que gobernaba y representaba a la ciudad en las Cortes. El concejo tenía gran poder sobre el alfoz y la ciudad. Sin embargo, no podía conceder cartas pueblas, es decir, dar título de villa a cualquier aldea (eso era potestad real, como la carta puebla de Añover de Tajo). Cabe aclarar que una villa es aquella población con capacidad de hacer justicia (juzgar, detener y ajusticiar e imponer penas), y se simboliza en los rollos o picotas de piedra (columnas donde se hacía justicia, e.g. ejecuciones).


== Origen y evolución ==

Entre las primeras Cartas Pueblas de que se tiene constancia están: en el reino astur-leonés, las de la de Obona, Valpuesta[cita requerida] y la Carta Puebla de Brañosera (concedidas respectivamente por Aldegastro, Alfonso el Casto y Nuño Núñez a comienzos del siglo IX); y en los condados catalanes, las de Freixá y Cardona (concedidas por Witardo y Borrel II en la segunda mitad del siglo X).
Con el título de fueros se fueron dando documentos a partir del siglo XI en León y Castilla, como el Fuero de León (1017), el Fuero de Sepúlveda (confirmado en 1076), los de Castrojeriz, Andaluz (Soria) y Burgos, el Fuero de Logroño, el Fuero de Miranda de Ebro, los de Segovia, Ávila y Salamanca; continuando en el siglo XII con el de Álava (1114 y confirmado en 1140)'Toledo (1118) o el de Medina del Campo (1181); y ya en el siglo XIII con los de Plasencia, Cáceres, Mérida, Montánchez o Badajoz en la extremadura leonesa,[1]​ Uclés, Madrid o Alcalá de Henares en el centro peninsular,[2]​ y los de las villas del señorío de Vizcaya (desde el de Balmaseda en 1199 hasta el de Bilbao en 1300). En 1342 se produjo el Primer ordenamiento del Fuero de Vizcaya (de la denominada Tierra Llana) y en 1394 el Fuero Viejo de las Encartaciones, que fueron objeto de diversas modificaciones (Fuero Viejo de 1452, Fuero Nuevo de las Encartaciones de 1526), siendo una de las más significativas el juramento regio («Lo que ha de jurar el rey e señor de Vizcaya e dónde e cómo»), que debía hacerse tres veces: «a las puertas de la villa de Bilbao (...) a Guernica, so el árbol donde se acostumbra facer Junta (...) [y en Bermeo] ante el altar de Santa Ufemia».[3]​
En los territorios pirenaicos de Navarra y Aragón hay fueros al menos desde el Fuero de Jaca (1076) (sin contar con el mítico Fuero de Sobrarbe, invención posterior que dio origen a la expresión «antes fueron leyes que reyes» para caracterizar el carácter del Reino de Aragón y postular una legendaria dinastía real originaria, cuyo emblema sería la cruz de gules sobre la encina, tras la aparición milagrosa de esta en un relato folclórico compartido con navarros y vascos), que se extendieron a los fueros navarros (Pamplona, Estella, Tudela) y guipuzcoanos (San Sebastián). A partir del Fuero de Zaragoza (1119) los fueros se extienden por el Bajo Aragón, donde son más tardíos, siendo los más relevantes los de Teruel y Albarracín, paralelos al de Cuenca en la corona castellana.
Al otro lado del Pirineo se otorgaron por los vizcondes de Bearne los Fòrs, que tendrán también influencia en algunas villas guipuzcoanas, con el nombre de Usos de Oloron.
En el reino de Portugal se extendieron en algunos casos los fueros leoneses y castellanos, como el fuero de Évora, extensión de uno previo de Ávila del que no se tiene apenas más noticia,[4]​ y que posteriormente se extendió a su vez a Palmela, Aljustrel y Setúbal.[5]​ Otros fueros son sanción de usos preexistentes, como el de Porto de Mós (1305).[6]​ El fuero de Lisboa es de 1227, y se extendió posteriormente a Ceuta.
Aunque siguieron otorgándose fueros en el siglo XIII, con el desplazamiento de la reconquista hacia el sur dejaron de tener su función original de estimular la repoblación de las tierras fronterizas más o menos despobladas del desierto del Duero o de las extremaduras. Las zonas reconquistadas a partir de entonces (el valle del Guadalquivir y las llanuras litorales de Valencia y Murcia) eran zonas con alto desarrollo urbano y gran densidad de población; y los instrumentos políticos ya eran otros (órdenes militares y huestes aristocráticas y concejiles de las ciudades de amplios alfoces ya desarrolladas del norte y centro peninsular), a los que había que compensar con repartimientos en los nuevos territorios conquistados.


=== Los fueros como registro idiomático ===
El uso del latín o de las lenguas romances difería en cada uno de los fueros y en cada una de sus versiones, traducciones o copias, muchas de ellas verdaderas falsificaciones o llenas de interpolaciones que desvirtuaban el contenido original para justificar todo tipo de pretensiones; lo que ha convertido a los documentos forales y las cartas pueblas en uno de los principales objetos de la crítica documental y la gramática histórica.
El Fuero de Avilés (1085) y el Fuero de Oviedo se consideran entre los textos más antiguos en asturleonés. El Fuero de Castro Caldelas (1228) es el documento más antiguo escrito en gallego que se conoce.


=== Clases de fueros ===
Los fueros municipales pueden ser breves (propio de los siglos IX al XI, como los de León, Jaca y Castrojeriz) o extensos (siglos XII en adelante, como el de Cuenca); agrarios o fronterizos (que incorporan más privilegios); principales (que se bastan a sí mismos) o suplementarios (que se remiten a los principales); tipos (o troncos) y extensiones (que toman a los tipos o troncos como modelos).[7]​


=== Familias de fueros ===
La historiografía ha establecido "familias de fueros" en función de la identidad y adaptación de su contenido al de un "tronco" que fue extendiéndose a muchas otras localidades, en cada uno de los reinos medievales peninsulares:[8]​
Fueros de Valencia
Fuero de León
Fuero de Sahagún
Fuero de Benavente
Fuero de Logroño
Fuero de Toledo
Fuero de Cuenca-Teruel (Fuero de Cuenca, Fuero de Teruel, Fuero de Teruel y Albarracín o Fuero de Albarracín, que son extensión del Fuero de Sepúlveda)[9]​
Fuero de Jaca o de Jaca-Estella
Fuero de Zaragoza
Fuero de Lérida
Fuero de Córdoba


=== Fueros generales ===
Todos los fueros locales tenían su raíz en el derecho consuetudinario (también se denominaban costumbres) y de su conjunto, unidos a las normas romanas y visigodas, se obtuvieron recopilaciones de ámbito territorial supramunicipal, dando lugar a distintos fueros generales en cada uno de los reinos cristianos peninsulares: Fuero de Aragón (compilado en el Vidal Mayor), Fuero General de Navarra, los fueros generales leoneses y castellanos (Fuero Juzgo, Fuero Real y Fuero Viejo de Castilla), los documentos catalanes de naturaleza similar (Usatges de Barcelona, Constitucions i altres drets de Catalunya) y su extensión en los Fueros de Valencia y las Franquesas, Franqueses o Carta de franquesa de Mallorca.[10]​


=== Trascendencia en la historia posterior de España ===

La importancia de los fueros traspasa el ámbito medieval, siendo una constante el poder movilizador del particularismo y los privilegios locales, en radical contradicción con el centralismo que suponía la construcción de la monarquía autoritaria a partir de la crisis bajomedieval.[cita requerida]
Ya en la Edad Moderna, la Guerra de las Comunidades de Castilla (1520-1522) tuvo en la defensa de los derechos forales por parte de los comuneros su primera causa de levantamiento, y su derrota a manos de los imperiales implicó que Castilla fuera a partir de entonces el territorio más sometido al poder de la Monarquía Hispánica. Los fueros del reino de Aragón fueron radicalmente recortados como consecuencia de la revuelta de Antonio Pérez (1590-1591); mientras que los catalanes, violentamente defendidos en la sublevación de 1640, fueron suprimidos en la parte que quedó en poder de Luis XIV de Francia y mantenidos en la mayor parte del territorio cuando retornó a manos de Felipe IV de España por el Tratado de los Pirineos (1659). Tanto los fueros de Cataluña como los de los demás reinos de la corona aragonesa (Valencia y Mallorca), fueron suprimidos a comienzos del siglo XVIII como consecuencia de su derrota en la Guerra de Sucesión (1700-1715), con los Decretos de Nueva Planta, a excepción del derecho civil foral catalán y aragonés.[cita requerida]


==== Navarra y el País Vasco, territorios forales ====
Únicamente los territorios vascos y el reino de Navarra (fieles a Felipe V, de la nueva dinastía Borbón, precisamente de origen navarro) continuaron manteniendo su particularidad foral (régimen fiscal y monetario propio, aduanas, exención del servicio militar, etc.), que volvió a suscitar conflictos en la Edad Contemporánea como consecuencia de las guerras carlistas, y se mantuvieron con distintas alternativas hasta su reformulación como autonomías según la Constitución de 1978 (Comunidad Foral de Navarra y el País Vasco).[cita requerida]
Tras la Tercera Guerra Carlista, mediante la ley de Madrid de 21 de julio de 1876 firmada por el rey Alfonso XII, los fueros quedaron derogados unilateralmente, excepto en lo referente a especialidades fiscales y tributarias, aunque este monarca nunca los había jurado para ser su Señor. En los años finales del siglo XIX surgió un movimiento nacionalista vasco en torno a Sabino Arana y el PNV; mientras que en Navarra se desarrolló un movimiento de defensa de la foralidad (gamazada de 1893). Para las tres provincias vascas, la autonomía política fue parcialmente recuperada por el Estatuto de Autonomía del País Vasco de 1936 redactado durante la Segunda República Española y que entró en vigor de forma precaria durante la Guerra Civil Española (1936-1939).
El franquismo, bando vencedor y particularmente definido por el totalitarismo en la definición del Estado, estaba además muy involucrado en la zona (Bilbao fue la capital económica durante la guerra y una de sus familias era la carlista), no sólo ignoró el estatuto, sino que suprimió las particularidades forales de las provincias traidoras de Vizcaya y Guipúzcoa, respetando los de las fieles Álava y Navarra.
Con la transición, la Constitución Española de 1978 reconocía la vigencia de los Derechos Históricos, y se redactaron los vigentes estatutos de autonomía: el Estatuto de Autonomía del País Vasco de 1979 y el Amejoramiento del Fuero navarro de 1982. Además, internamente el País Vasco se organiza en tres diputaciones forales, con amplísimas competencias.


== Referencias ==


== Bibliografía ==
Ana María (1989). «El Derecho local en la Edad Media y su formulación por los reyes castellanos». Anales de la Universidad de Chile 5ª (20). p 105-130. Estudios en honor de Alamiro de Avila Martel. 
Barrero García, Ana María y Alonso Martín, María Luz (1989). Textos de Derecho local español en la Edad Media. Catálogo de Fueros y Costums municipales. Madrid: Consejo Superior de Investigaciones Científicas. Instituto de Ciencias Jurídicas. ISBN 84-00-06951-X. 
Barrientos Grandon, Javier (1994). Introducción a la historia del Derecho chileno. I. Derechos propios y Derecho común en Castilla. Santiago: Barroco Libreros. 
García-Gallo, Alfonso (1956). «Aportación al estudio de los Fueros». Anuario de Historia del Derecho Español 26 (.). p 387-446. 


== Véase también ==
Carta Puebla
Derecho foral
Derecho foral español
Fazañas
Régimen foral
Fueros navarros
Fuero General


== Enlaces externos ==
 Wikimedia Commons alberga una categoría multimedia sobre fuero.