Eine Flussanzapfung entsteht, wenn sich ein Flusslauf zu einem anderen hin verlagert und diesem (im wörtlichen Sinne) „das Wasser abgräbt“. Die Wasserscheide wird hierbei durchbrochen und verlagert sich: Der angezapfte Fluss verkümmert unterhalb der Anzapfungsstelle, dort kann ein Trockental entstehen, während der andere seine Wassermenge mit dem Fremdwasser vermehrt.
Eine ungewöhnlich starke Flussabbiegung kann als Hinweis auf einen derartigen Vorgang gedeutet werden – man bezeichnet sie als Anzapfungsknie.


== Anzapfung und Ablenkung ==
Als Anzapfung im strengen Sinne werden nur relativ plötzliche Ereignisse gesehen, langfristigere geodynamische Prozesse und deren hydrographischen Auswirkungen fasst man in der moderneren Literatur unter dem Ausdruck Flussablenkung zusammen. Tatsächlich ist die „Plötzlichkeit“ in geologischen Zeiträumen immer mit Vorbehalt zu betrachten und umfasst meist mehrere Zwischenstadien.


== Ursachen ==
Für die Flussanzapfung lassen sich mehrere Ursachen festmachen, die in der geologischen Entwicklung ineinander übergreifende Prozesse darstellen können, die zu den heutigen Flussläufen geführt haben:

 Tektonische Hebung oder Senkung: Ein Areal wird beispielsweise so gehoben, dass der Fluss dieses nicht mehr überwinden kann.
 Aufschotterung: Durch verringerte Fließgeschwindigkeit oder Mäanderung lagert sich im Flussbett derart viel Schotter ab, dass es gehoben wird und schließlich ein bisher unüberwindbares Hindernis (Wasserscheide) überflossen werden kann (siehe z. B. Feldbergdonau).
 Rückschreitende Erosion: Durch die Materialabführung schneidet sich der Fluss zu seiner Quelle hin in den Untergrund ein, was in besonderen Fällen zur Flussanzapfung und einem Strunkpass führen kann.
 Gletscherstau: Hierbei bilden sich meist Gletscherstauseen, die über eine Erhebung hin ausbrechen und ihr neues Durchbruchstal etablieren, bevor sie sich durch Gletscherrückgang wieder zurückbilden.
 Karstversickerung: Abzapfung im Quellhorizont. Im Untergrund sickert Wasser in ein fremdes Quellgebiet.
 Moorversickerung: In großflächigen flachen Flusssümpfen und -auen, die nicht in einer orographischen Senke liegen, können echte Bifurkationen entstehen. Tektonische oder erosive Kräfte können daraus später echte Anzapfungen formen.Eine „moderne“ Form tritt im Wasserkraftwerksbau auf, wo durch Anzapfung Wasser eines Einzugsgebietes einem fernen Krafthaus zugeleitet wird. Einem Austrocknen des Flusses kann nur durch Restwassergabe entgegengewirkt werden. Im Kraftwerksbau wird diese gesetzlich oder vertraglich verankert; wo sie fehlt (Großprojekte außerhalb der Industrienationen), können großräumige hydrologische Veränderungen auftreten.


== Beispiele ==
 Wutachablenkung der Urdonau (Feldbergdonau) in der Wutachschlucht, deren Anzapfungscharakter nicht sicher ist
 Donauversinkung bei Tuttlingen, wo Wasser im Untergrund in den Aachtopf zum Rhein abgezapft wird
 Paardurchbruch bei Friedberg-Ottmaring, wo die ursprünglich parallel zum Lech verlaufende Paar nach Nordosten abknickt und das Lechtal verlässt
 Versickerung der Hönne im Sauerland
 Donknie und Wolgaknie bei Wolgograd durch Hebung der Wolgaschwelle und der Jergeni-Hügel relativ zur Kaspischen Senke, heute durch den Wolga-Don-Kanal überbrückt


== Literatur ==
 Joachim Mangelsdorf, Karl Scheurmann: Flußmorphologie. Ein Leitfaden für Naturwissenschaftler und Ingenieure. Oldenbourg, 1980, ISBN 3-486-23311-4