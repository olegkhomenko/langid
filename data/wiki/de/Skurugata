Skurugata (Skurustraße) ist eine Felsformation in der Gemeinde Eksjö, Jönköpings län in Süd-Schweden. Sie bildet einen 800 Meter langen Canyon, der etwa 20 bis 50 Meter tief und 7 bis 24 Meter breit ist. Skurugata liegt etwa 15 km nordöstlich von Eksjö.
Verschiedene Theorien zur Entstehung dieser eigenartigen Formation wurden aufgestellt, von denen eine davon ausgeht, dass eine parallele Faltung stattgefunden hat. Das Gestein in der Schlucht besteht größtenteils aus graubraunem und dunkelbraunem Porphyr mit Anteilen an Quarz und Rotem Feldspat. Verschiedene Arten von Moosen finden sich in der Schlucht und auch außerhalb.
Die Schlucht ist allgemein begehbar. In der frühen Zeit des Jahres und nach starkem Regen füllen sich die tieferen Stellen mit Wasser. Die Temperatur am Boden der Schlucht ist typischerweise etwas tiefer als in den außen gelegenen Gebieten. Etwa in der Mitte der Schlucht ist ein loser, mit einer Markierung versehener Stein in die Wand eingelassen, hinter dem sich ein Loch mit einem Gästebuch befindet.
Die Gegend der Skurugata ist seit 1967 als Naturreservat mit einer Größe von etwa 42 Hektar ausgewiesen, deshalb gilt hier nicht das schwedische Jedermannsrecht und Campen ist hier nicht gestattet. Daneben sind Motorfahrzeuge nicht erlaubt und eigener, mitgebrachter Abfall muss entfernt werden. Ein Wanderweg führt von der Schlucht hinauf auf den 337 Meter hohen Skuruhatt, von dem man einen schönen Ausblick auf die bewaldete Landschaft Smålands hat.
Die Skurugata wurde bereits 1696 von Erik Dahlberg in seiner Suecia Antiqua et Hodierna beschrieben und abgebildet. Auch der Autor Albert Engström schilderte die Gegend eindrucksvoll.


== Literatur ==
 Länsstyrelsen Naturvårdsverket: Skurugata. Örebro: StaLan grafiska.


== Weblinks ==