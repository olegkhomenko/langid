Merkur war der Name einer vom Ford-Konzern in den Jahren 1984 bis 1990 geführten Automobilmarke, die deutsche Ford-Modelle in Nordamerika anbot. Der Vertrieb der Merkur-Fahrzeuge erfolgte über die Mercury-Händler.
Verkauft wurden:

 der Merkur XR4Ti (1984–1989) 
 der Merkur Scorpio (1987–1990)


== Merkur XR4Ti ==

Bei diesem Fahrzeug handelte es sich um eine im Spätsommer 1984 eingeführte Version des dreitürigen Ford Sierra (von Ford als Coupé bezeichnet), ausgerüstet mit einem 2,3 Liter großen Vierzylinder-Turbo-Motor, den Ford USA entwickelt hatte und auch in anderen Fahrzeugen, etwa dem Ford Mustang, einsetzte. Der überquadratisch ausgelegte Motor mit Benzineinspritzung leistete im Merkur mit Fünfgang-Schaltgetriebe 130 kW (177 PS) (diese Motorversion entsprach der in der Mustang-Hochleistungsversion SVO eingesetzten Maschine), im XR4Ti mit Dreigang-Automatik war er auf 108 kW (147 PS) gedrosselt.
Ab Mitte 1987 erhielt der XR4Ti einen einfachen statt des zuvor verwendeten Doppel-Heckspoilers und Stoßstangen in Wagenfarbe.
Die Preise beliefen sich auf US-$ 16.361 bis 19.065, der Normverbrauch auf 12,4 bzw. 9,8 Liter/100 km (Stadt/außerorts).
Von Herbst 1984 bis Mitte 1987 wurden 37.590 Merkur XR4Ti verkauft, zwischen Mitte 1987 und Ende 1989 belief sich der Absatz aller Merkur-Modell gemeinsam auf 24.026 Stück, wovon etwa die Hälfte auf den XR4Ti entfielen. Gebaut wurde der XR4Ti bei der Firma Karmann in Rheine.


== Merkur Scorpio ==

Im Mai 1987 führte Merkur als zusätzliches Modell den US-$ 24.048 teuren Scorpio ein, der auf dem fünftürigen Ford Scorpio aufbaute. Der Scorpio besaß den auch in Europa erhältlichen 2,9-Liter-V6, der hier 107 kW (146 PS) leistete, und war mit Klimaanlage und elektrischer Sitzverstellung betont komfortabel ausgestattet. Ein Fünfgang-Schaltgetriebe war ebenso erhältlich wie eine Viergang-Automatik.
Merkur verkaufte von Mitte 1987 bis Ende 1989 insgesamt 24.026 Autos (inklusive XR4Ti), 1990 kamen weitere 2.622 Scorpio hinzu.
Der Verkauf der Merkur-Modelle litt unter den stark schwankenden Wechselkursen. Hinzu kam, dass die Mercury-Händler mit dem Mercury Sable ein Modell anboten, das bei gleicher Größe und ähnlichem Karosseriestil deutlich preisgünstiger war als der importierte Scorpio. Die Marke „Merkur“ wurde deshalb im Sommer 1990 ersatzlos aufgegeben.


== Quellen ==
 Covello, Mike: Standard Catalog of Imported Cars 1946–2002. Krause Publications, Iola 2002. ISBN 0-87341-605-8, S. 557.