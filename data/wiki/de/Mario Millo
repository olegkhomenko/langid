Mario Millo (* 17. Mai 1955 in Sydney, New South Wales als Mario Daniel Millo) ist ein australischer Musiker, Komponist und Songwriter. Er komponierte unter anderem die Musik zu den Kinofilmen The Lighthorsemen, Meine Pferde – meine Liebe und P.C. – Ein Genie auf Pfoten


== Leben und Werk ==
Mario Millo wurde 1955 in Sydney, New South Wales als Sohn italienischer Einwanderer in Australien geboren. Im Alter von fünf Jahren lehrte ihn sein Vater Mandoline zu spielen, mit acht Jahren lernte er Gitarre. Mit zwölf Jahren hatte Millo seine erste eigene Band namens The Menu an der Mitchell High School in Blacktown. The Menu bestand aus: Mario Millo (Gitarre und Gesang), Mark Friedland (Schlagzeug), Vince Moult (Bass) und Brian Nicholls (Orgel). Die Band gewann 1969 den Pepsi Pop Poll der beim"Battle of the Bands im Sydney Stadium stattfand. Als Ergebnis unterzeichneten sie einen Vertrag mit einer Musik-Agentur in Sydney und wurden umbenannt in The Clik. Eine Single names La De Da erschien auf Festival Records im November 1969. Eine zweite Single, Mary Mary, wurde im März 1970 veröffentlicht. Coca-Cola und Fanta sponserte die Band in einer Reihe von Fernsehspots und man schickte die junge Band auf Tourneen durch Australien und Neuseeland. 1971 löste sich die Band dann auf. Millo bereiste für ein halbes Jahr Europa und kehrte Anfang 1972 nach Australien zur reformierten Band The Clik zurück. Nun mit Garry Adams an der Leadgitarre, Doug Bligh am Schlagzeug (beide Ex-Galadriel) und Phil Cogan am Bass. Die Band trennte sich aber im Oktober 1973 wieder. Danach wurde Millo Mitglied der Symphonic Rock Gruppe Sebastian Hardie. Das Debütalbum der Gruppe namens Four Moments (1975) erreichte Platz 13 der Australian Kent Music Report Albums Charts. Die Auflösung von Sebastian Hardie folgte im Jahr 1977.
Millo machte anschließend eine Solo-Karriere und komponierte Film- und Fernseh-Soundtracks und Scores. Im Jahr 1978 arbeitete er gemeinsam mit Jon English an dem Soundtrack für die Fernsehserie Against the Wind. Eine Single namens Six Ribbons wurde ausgekoppelt. Sowohl Album als auch die Single erreichten Top 10 Plätze auf den entsprechenden Kent Music Report Charts. Die Serie lief international. Als Mot alla vindar wurde die Single 1980 vor allem in den Ländern Skandinaviens bekannt, wo das Album und die Single Platz 1 in der Liste der Nummer-eins-Hits in Norwegen und Platz 4 in Schweden erreichte.
Millos Kompositionen für Filme und Fernsehserien haben in Australien einige Auszeichnungen gewonnen, unter anderem den begehrten Australian Film Institute Award für Simon Wincers The Lighthorsemen.
Für Brides of Christ gewann er 1992 den Australian Record Industry Association Award in der Kategorie Best Original Soundtrack Album. Millo wurde für den gleichen Preis mehrmals ausgezeichnet. 1997 für G. P. und 2002 nochmals für Changi.
Mario Millo lebt und arbeitet in New South Wales, Australien.


== Auszeichnungen ==
 1988: AACTA Award in der Kategorie „Best Original Music Score“ für The Lighthorsemen
 1992: ARIA Award in der Kategorie „Best Film Score“ für Brides of Christ
 1997: ARIA Award in der Kategorie „Best Television Theme“ für G. P.
 1999: AGSC Award in der Kategorie „Best Original Music in a Children’s TV or Animation Series“ für See How They Run
 2002: ARIA Award in der Kategorie „Best Music for a Mini-Series or Telemovie“ für Changi


== Filmografie ==
Kinofilme

 1987: The Lighthorsemen
 1988: Schande (Shame)
 1989: Meine Pferde – meine Liebe (Minnamurra)
 1996: Unter Mordverdacht (Natural Justice: Heat)
 1997: P.C. – Ein Genie auf Pfoten (Paws)Fernsehfilme

 1980: People Like Us
 1986: Body Business
 1993: Singapore Sling
 1993: Joh’s Jury
 1995: Singapore Sling: Road to Mandalay
 1995: Singapore Sling: Old Flames
 1999: See How They Run
 2002: Heroes’ Mountain – Eisige Hölle (Heroes’ Mountain)Miniserien

 1978: Against the Wind
 1988: Spit MacPhee
 1989: Tanamera – Lion of Singapore
 1991: Brides of Christ
 2001: ChangiFernsehserien

 1985: A Fortunate Life (Vier Folgen)
 1988: Gefangen im Paradies (Always Afternoon)
 1995: Natural Justice
 2009: McLeods Töchter (McLeod’s Daughters, zwei Folgen)Dokumentarspielfilme

 1984: Aussie AssaultKurzfilme

 2014: The Last Days of Ben Hall 


== Diskografie ==
Filmmusik

 1978: Against the Wind (Polydor Records)
 1984: World Safari (Powderworks Records)
 1986: A Fortunate Life (Polydor Records)
 1987: The Lighthorsemen (DRG Records)
 1988: Shame (DRG Records)
 1989: GP (Festival Records) mit Simon Walker
 1991: Brides Of Christ (ABC Music)
 1997: Paws (Larrakin Records)
 2001: Changi (Universal Music Australia)Studio Alben

 1979: Epic III (Mario Millo) (Polydor Records)
 1983: Human Games (Mario Millo) (EMI Records)
 2002: Oceans of the Mind (Mario Millo) (Red Moon Music)


== Literatur ==
 Ian McFarlane: Mario Millo. In: The encyclopedia of Australian rock and pop. Allen & Unwin, St. Leonards, NSW 1999, ISBN 1-86508-072-1. 


== Weblinks ==
 Offizielle Webseite von Mario Millo
 Mario Millo in der Internet Movie Database (englisch)
 Mario Millo bei Discogs (englisch)
 Mario Millo bei Allmusic (englisch)
 Filmografie und Diskografie von Mario Millo bei Soundtrackcollector


== Einzelnachweise ==