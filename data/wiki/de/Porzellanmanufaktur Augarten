Die Porzellanmanufaktur Augarten ist eine Wiener Porzellanmanufaktur; gegründet wurde sie in ihrer heutigen Form 1923.
Benannt ist sie nach dem Augarten, jenem Park, in dem ihre Manufakturgebäude (Schloss Augarten und ein in der Zweiten Republik errichteter Erweiterungsbau) Nachbarn des Palais Augarten der Wiener Sängerknaben sind.


== Bedeutung ==
Von der kunsthistorischen Bedeutung her ist Augarten Porzellan mit Meißen und der Porzellanmanufaktur Nymphenburg vergleichbar. Die Stücke werden in der Manufaktur im 2. Wiener Gemeindebezirk Leopoldstadt händisch hergestellt.
Die Produkte von Augarten sind das teuerste österreichische Porzellan und werden auch für Staatszwecke wie in der Hofburg, dem Bundeskanzleramt und österreichischen Botschaften, sowie als Staatsgeschenke verwendet. Die Produktpalette umfasst Vasen, Speise-, Tee- und Kaffeeservices, Figuren, Lampen und Geschenke. Die Entwürfe entstammen verschiedenen Epochen und umfassen unterschiedliche Motive. Neben Chinoiserien, Blumen-Buketts, Jagd- und Naturmotiven stehen Klassizismus, Biedermeier, Art Déco und Moderne zur Auswahl.


== Geschichte ==
Nach Ende der Donaumonarchie und der Stabilisierung der Nachkriegswirtschaft wurde am 2. Mai 1923 im Schloss Augarten die „Wiener Porzellanmanufaktur Augarten“ (Porzellanfabrik Augarten A.-G.) im Beisein von Bundespräsident Michael Hainisch eröffnet. Das Unternehmen bezieht sich auf die Tradition der ehemaligen, 1864 geschlossenen Wiener Porzellanmanufaktur und verwendet teilweise deren Designs.
Das Art Déco war in dieser Zeit sehr beliebt und die Produkte wurden von Künstlern wie Franz von Zülow, Josef Hoffmann und Michael Powolny entworfen. Die Manufaktur produziert nach wie vor Altbewährtes aus allen Zeitrichtungen. Künstler wie Friedrich Ludwig Berzeviczy-Pallavicini, Thomas Feichtner, Walter Bosse, Yu Feng, Joseph Nigg, Gottfried Palatin, Ena Rottenberg, Claudia Stuhlhofer Mayr und Robert Ullmann sorgen für immer neue Entwürfe.
2003 - nachdem die Manufaktur wegen Konkurs geschlossen und das gesamte Personal gekündigt wurde - kaufte die Firma VMS Value Management Services GmbH des Sanierers Erhard Grossnigg die Konkursmasse auf und gründete damit eine neue Firma namens Neue Porzellanmanufaktur Augarten. Die Porzellanmanufaktur Augarten sei das einzige Investment (neben dem Linzer Fußballclub Lask), das Grossnigg „aus reiner Leidenschaft und ohne Profitinteresse“ betreibe.In einem Seitenflügel befindet sich seit dem Jahr 2011 das Porzellanmuseum im Augarten.
Im Jahr 2014 gab die Manufaktur in Zusammenarbeit mit der Österreichischen Post die erste Porzellan-Briefmarke der Welt heraus.


== Signierung ==
Auf jedem Produkt von Augarten wird der blaue Bindenschild aufgestempelt. Außerdem wird jedem Porzellanobjekt eine Maler-, Dekor- und Formnummer aufgetragen.


== Filialen ==
Augarten unterhält Filialen in Wien, Linz und Salzburg. Das Schloss selbst steht zur Besichtigung offen. Besucher können dort bei täglichen Führungen einen Einblick in die Produktionsweise von Augarten Porzellan gewinnen. Im neu gestalteten Porzellanmuseum können Besucher anhand repräsentativer Exponate die Geschichte des Wiener Porzellans erleben.
Die Produkte sind nicht nur in Österreich begehrt, auch bei Japanern finden sie großen Absatz. Im Flagshipstore in der Spiegelgasse 3 gibt es extra eine japanische Kundenbetreuerin. Im Kaiserpalast von Tokio wurde Augarten-Porzellan ebenfalls gesichtet.


== Galerie ==

		
		
		
		


== Siehe auch ==
 Liste von Porzellanmanufakturen und -herstellern


== Literatur ==
 Reinhard Engel, Marta Halpert: Luxus aus Wien II. Czernin Verlag, Wien 2002, ISBN 3-7076-0142-0.
 Waltraud Neuwirth: Die Wiener Porzellan-Manufaktur Augarten. Jugend und Volk, Wien 1996, ISBN 3-224-18867-7.
 Waltraud Neuwirth: Wiener Porzellan. Original, Kopie, Verfälschung, Fälschung. Jugend und Volk, Wien 1996, ISBN 3-224-18867-7.
 Waltraud Neuwirth: Porzellan aus Wien. Von du Paquier zur Manufaktur im Augarten. Jugend und Volk, Wien 1992, ISBN 3-8113-6084-1.
 Waltraud Neuwirth: Wiener Porzellan. 1744–1864. Neuwirth, Wien 1983, ISBN 3-900282-11-0.
 Wilfried Seipel (Hg.): Weißes Gold aus Europa. Ausstellungskatalog, Kunsthistorisches Museum Wien 1997, ISBN 3-900325-79-0.
 Wilhelm Siemens: Impulse – Europäische Porzellanmanufakturen als Wegbereiter internationaler Lebenskultur. Zweckverband Deutsches Porzellanmuseum, 1995, ISBN 3-927793-43-4.


== Auszeichnungen ==
 2014: Österreichischer Museumspreis – Anerkennungspreis


== Film ==
 Augarten Porzellan. Dokumentarfilm, Österreich, 2017, 30 Min., Regie: Patrick Pleisnitzer, Moderation: Karl Hohenlohe, Produktion: Clever Contents, ORF, Reihe: Aus dem Rahmen, Erstsendung: 28. November 2017 bei ORF III, Inhaltsangabe von ORF.


== Weblinks ==

 Wiener Porzellanmanufaktur Augarten
 Eintrag zu Wiener Porzellanmanufaktur Augarten Ges. m. b. H. im Austria-Forum (in AEIOU Österreich-Lexikon)
 Eintrag zu Augartenporzellan im Austria-Forum (im Heimatlexikon)
 Augarten Japan. Archiviert vom Original am 5. März 2008; abgerufen am 22. November 2009. 


== Einzelnachweise ==