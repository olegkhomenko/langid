Laurent Stalder (* 1970 in Lausanne) ist ein Schweizer Architekt und Hochschullehrer.


== Leben ==
Nach seinem Studium der Architektur an der ETH Zürich, das er 1996 mit dem Diplom abschloss, war er in den Jahren 1996 und 1997 Stipendiat des Schweizerischen Instituts für Ägyptische Bauforschung und Altertumskunde in Kairo. Von 1997 bis 2001 war Stalder Assistent am Institut für Geschichte und Theorie der Architektur (gta) des Departments Architektur der ETH Zürich. Dort promovierte er 2002 über Hermann Muthesius. Im gleichen Jahr wurde er an die Université Laval in Québec, Kanada zum Assistenzprofessor für Architekturgeschichte berufen. 2006 kehrte er als Assistenzprofessor für Architekturtheorie an das Institut gta der ETH Zürich zurück. Seit 2011 hat er dort die Professur für Architekturtheorie inne. 2009 war er Visiting Scholar am Massachusetts Institute of Technology in Cambridge, Mass.
2016 wurde Laurent Stalder Vorsteher des Instituts für Geschichte und Theorie der Architektur der ETH Zürich.


== Mitgliedschaften ==
Laurent Stalder ist Stiftungsratsmitglied des Schweizerischen Architekturmuseums in Basel und der Stiftung Bibliothek Werner Oechslin in Einsiedeln. Er ist wissenschaftlicher Beirat des Jaap Bakema Study Centre in Rotterdam und des Laboratoire Infrastructure, Architecture, Territoire (LIAT) der ENSA Paris-Malaquais.


== Veröffentlichungen ==


=== Autor ===
 Hermann Muthesius 1861–1927. Das Landhaus als kulturgeschichtlicher Entwurf. gta Verlag, Zürich 2008, ISBN 978-3-85676-219-3.


=== Herausgeber ===
 Moritz Gleich, Laurent Stalder (Hg.), gta papers 1. Architecture / Machine. gta Verlag, Zürich 2017, ISBN 978-3-85676-363-3.
 Laurent Stalder, Georg Vrachliotis (Hg.), Fritz Haller. Architekt und Forscher.  gta Verlag, Zürich 2015, ISBN 978-3-85676-334-3.
 Laurent Stalder, Cornelia Escher, Megumi Komura, Meruro Washida (Hg.). Atelier Bow-Wow. A Primer. Verlag der Buchhandlung Walther König, Köln 2013, ISBN 978-3-86335-302-5.
 Alessandra Ponte, Laurent Stalder, Tom Weaver (Hg.). God & Co. François Dallegret. Beyond the Bubble. AA Publications, London 2011, ISBN 978-1-907896-18-7.
 Laurent Stalder, Anke Hagemann, Elke Beyer, Kim Förster (Hg.), ARCH+ 191/192 (2009): Schwellenatlas. Von Abfallzerkleinerer bis Zeitmaschine. ISBN 978-3-931435-20-2.
 Laurent Stalder (Hg.), Valerio Olgiati. Verlag der Buchhandlung Walther König, Köln 2008, ISBN 978-3-86560-491-0.


== Beleg ==
 Curriculum Vitae an der ETH Zürich