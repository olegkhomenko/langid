Hieronymus Hartwig Moller (* 14. Juli 1641 in Hamburg; † 6. Dezember 1702 ebenda) war ein deutscher Jurist, Ratsherr und Bürgermeister von Hamburg.


== Herkunft und Familie ==
Moller stammte aus dem Hamburger Hanseatengeschlecht Moller vom Baum. Zur Unterscheidung von anderen gleichnamigen Familien nannte diese Familie sich nach dem Hülsenbaum in ihrem Wappen vom Baum.
Mollers Eltern waren der Hamburger Ratssyndicus Johann Moller († 1672) und dessen zweite Ehefrau Cäcilia von Spreckelsen, Tochter von Hartwig von Spreckelsen. Der Hamburger Bürgermeister Barthold Moller (1605–1667) war sein Onkel.
Moller heiratete im Jahr 1673 Anna Margaretha Schmidt († 1691), Tochter des Juraten an Sankt Petri Johann Schmidt. Von seinen sieben Kindern wurde Hartwig Johann Moller (1677–1732) Hamburger Oberaltensekretär und Ratsherr.


== Leben ==
Moller besuchte das Akademische Gymnasium in Hamburg und im Jahr 1660 das Gymnasium illustre in Bremen. Er studierte ab 1662 Jurisprudenz unter Johannes Friedrich Böckelmann (1633–1681) an der Ruprecht-Karls-Universität Heidelberg. Später ging er an die Universität Straßburg und schloss sein Studium 1665 als Lizenziat beider Rechte ab. Nach seinem Studium bereiste er vier Jahre lang Deutschland, Frankreich, Italien, Belgien, die Niederlande und England.
Im Jahr 1669 kehrte er nach Hamburg zurück und ließ sich hier als Advokat nieder. Am 2. Juni 1670 kam er in den Besitz der Familienvikarie. Im Jahr 1677 wurde er Richter am Hamburger Niedergericht, 1678 auch Präses und 1679 Aktuar am Niedergericht. Am 2. Mai 1682 wurde er zum Ratsherrn gewählt und war von 1688 bis 1694 Amtmann in Ritzebüttel. Zurück in Hamburg wurde er am 10. März 1697 für den verstorbenen Johann Schulte (1621–1697) zum Bürgermeister gewählt. Moller starb 1702 im Alter von 61 Jahren an Schwindsucht.


== Wirken ==
Mollers Amtszeit fiel in eine unruhige Zeit in der die Hamburgische Bürgerschaft und der Rat in ständiger Uneinigkeit sich gegenüber standen. Als Ratsherr nahm Moller 1685 während der Unruhen um Hieronymus Snitger (1648–1686) und Cord Jastram (1634–1686) an der Gesandtschaft nach Berlin teil. Diese Gesandtschaft sollte Kurfürst Friedrich Wilhelm von Brandenburg (1620–1688) um Hilfe bei der Vermittlung zwischen Hamburg und dem Herzog Georg Wilhelm von Braunschweig-Lüneburg-Celle bitten. Der Kurfürst schickte daraufhin seinen Staatsrat Friedrich von Canitz (1654–1699) zur Vermittlung nach Hamburg. Moller reiste 1685 auch wegen der gleichen Angelegenheit an den kaiserlichen Hof nach Wien. Hier wurden er und der nachherige Hamburger Bürgermeister Johann Diedrich Schaffshausen (1643–1697) von dem Cellischen Hofrat und Gesandten Asche Christoph von Marenholtz (1645–1713) öffentlich beleidigt und angegriffen. Marenholtz fiel daraufhin bei Kaiser Leopold I. (1640–1705) in Ungnade und musste seine diplomatische Karriere beenden. Moller und Schaffshausen kehrten erst nach Hamburg zurück als Dänemark am 26. August 1686 Hamburg angegriffen hatte, dieser Angriff aber mit Unterstützung von den Cellern abgewehrt werden konnte, und nachdem die Unruhestifter Snitger und Jastram hingerichtet worden waren.


== Werke (Auswahl) ==
 Disputatio Iuridica Prima Continens Fundamenta ac Celebriores Quaestiones ex singulis Titulis priorum Librorum Pandectarum. Wyngaerden, Heidelberg 1663 (Online bei Google Books). 
 Dissertation juridica 5ta continens fundamenta et praecipuas questiones ex parte tertia Pandectarum, praes. Joh. Frid. Böckelmanno. Heidelberg 1664. 
 Disputatio inauguralis de jure retentionis. Walter, Heidelberg 1665 (Online bei Google Books). 


== Literatur ==
 Johann Albert Fabricius: Leichenrede anno 1702 für Hieronymus Hartwig Moller. Hamburg 1702 (Online bei Google Books). 
 Friedrich Georg Buek: Genealogische und Biographische Notizen über die seit der Reformation verstorbenen hamburgischen Bürgermeister. Johann August Meißner, Hamburg 1840, S. 149–151 (Digitalisat bei Google Books). 
 Ulrich Philipp Moller: Die hamburgische Familie Moller. Fabricius, Hamburg 1856, S. 55 (Online bei Google Books). 
 Hans Schröder: Lexikon der hamburgischen Schriftsteller bis zur Gegenwart. Band 5, Nr. 2656. Perthes-Besser & Mauke, Hamburg 1870 (Faksimile auf den Seiten der Staats- und Universitätsbibliothek Hamburg). 
 Eduard Lorenz Lorenz-Meyer u. Oscar Louis Tesdorpf: Hamburgische Wappen und Genealogien. Hamburg 1890, S. 270 (Digitalisat auf den Seiten der Staats- und Universitätsbibliothek Hamburg). 
 Bernhard Koerner (Hrsg.): Genealogisches Handbuch Bürgerlicher Familien. Band 18. C. A. Starke, Görlitz 1910, S. 305–306 (Digitalisat im Internet Archive – zugl. Hamburger Geschlechterbuch. Band 1. C. A. Starke, Görlitz 1910.). 


== Einzelnachweise ==