Michael George Aschbacher (* 8. April 1944 in Little Rock, Arkansas) ist ein US-amerikanischer Mathematiker, der eine führende Rolle im Programm der Klassifikation der endlichen einfachen Gruppen hatte, das in den 1980er Jahren einen vorläufigen Abschluss fand. Er beschäftigt sich mit der Theorie endlicher Gruppen, Kombinatorik und algebraischen Gruppen.
Aschbacher studierte am Caltech (Bachelor 1966) und an der University of Wisconsin–Madison, wo er 1969 bei Richard Bruck promoviert wurde (Collineation Groups of Symmetric Block Designs). 1969/70 war er als Post-Doktorand an der University of Illinois. Ab 1970 war er wieder am Caltech, wo er 1976 eine volle Professur erhielt. Er ist zurzeit „Shaler Arthur Hanisch Professor of Mathematics“ am Caltech.
Aschbacher war eine treibende Kraft im Klassifikationsbeweis der einfachen endlichen Gruppen. Eine noch verbliebene Lücke im Beweis, den Fall der „quasithin groups“, schloss er 2004 mit Stephen D. Smith. Allein diese Arbeit umfasst rund 1300 Seiten.
2012 erhielt er mit Richard Lyons, Stephen D. Smith und Ronald Solomon den Leroy P. Steele Prize für ihr Buch Classification of finite simple groups: Groups of characteristic 2-type (AMS 2011), das als Fortsetzung und Aktualisierung von Daniel Gorensteins Übersicht über das Klassifikationsprogramm von 1983 gedacht war (und das Programm am Beispiel von Gruppen der Charakteristik 2 darstellt).
1980 erhielt er den Colepreis in Algebra (für seine Arbeit A characterization of Chevalley groups over fields of odd order). 1990 wurde er in die National Academy of Sciences gewählt und er ist seit 1992 Mitglied der American Academy of Arts and Sciences. Im Jahre 2011 wurde ihm der Rolf-Schock-Preis für Mathematik von der Königlich Schwedischen Akademie der Wissenschaften in Stockholm zuerkannt. Er erhielt 2012 den Wolf-Preis in Mathematik (gemeinsam mit Luis Caffarelli) zugesprochen. 1978 war er Invited Speaker auf dem Internationalen Mathematikerkongress in Helsinki (A survey of the classification program of finite simple groups of even characteristic).
1996 bis 1998 war er Vizepräsident der American Mathematical Society (AMS), deren Fellow er ist.


== Schriften ==
 mit Stephen D. Smith: The classification of quasithin groups I and II. AMS, 2004, ISBN 0-8218-3410-X und ISBN 0-8218-3411-8.
 Finite group theory. Cambridge University Press 2000, ISBN 0-521-78675-4.
 Sporadic groups. Cambridge University Press 1994, ISBN 0-521-42049-0.
 3-Transposition groups. Cambridge University Press 1997, ISBN 0-521-57196-0.
 The finite simple groups and their classification. Yale University Press 1980, ISBN 0-300-02449-5.
 Overgroups of Sylow subgroups in sporadic groups. Memoirs of the AMS, 1986, ISBN 0-8218-2344-2.
mit Lyons, Smith, Solomon Classification of finite simple groups: groups of characteristic 2-type, AMS 2011


== Weblinks ==

 Aschbacher beim Mathematics Genealogy Project
 Webseite am Caltech
 Aschbacher: The Status of the Classification of the Finite Simple Groups. Notices AMS 2004, PDF, online, (englisch).
 Biographie anlässlich des Steele Preises 2012,Notices AMS, April 2012, PDF, online, (englisch)