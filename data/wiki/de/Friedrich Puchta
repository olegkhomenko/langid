Friedrich Puchta (* 24. November 1883 in Hof (Saale); † 17. Mai 1945 in München) war ein deutscher Politiker (USPD, SPD).


== Leben und Wirken ==


=== Leben im Kaiserreich (1883 bis 1919) ===
Puchta wurde 1883 als Sohn eines jüdischen Arbeiters geboren. Er besuchte die Volksschule in Hof in Bayern und erlernte anschließend das Textilarbeiterhandwerk, in dem er die nächsten zehn Jahre lang tätig war. 1903 trat Puchta in die Sozialdemokratische Partei Deutschlands (SPD) und in die Gewerkschaft ein. Aus Puchtas 1905 geschlossener Ehe ging mindestens eine Tochter, die Mutter des SPD-Politikers Fred Gebhardt, hervor.
Von 1907 bis 1908 besuchte Puchta die SPD-Parteischule in Berlin. Anschließend arbeitete er als Redakteur in der sozialdemokratischen Presse. Im Herbst 1908 übernahm Puchta die redaktionelle Leitung der sozialdemokratischen Zeitung in Bayreuth. In den Jahren 1911 bis 1914 amtierte Puchta als Gemeindebevollmächtigter (Stadtverordneter) in Bayreuth. Hinzu kam die Tätigkeit als Lehrer auf volkswirtschaftlichem und wirtschaftsgeschichtlichem Gebiet an verschiedenen Volkshochschulen in Sachsen.
Ab 1914 nahm Puchta am Ersten Weltkrieg teil. Während des Krieges schloss er sich der USPD an, einer sich aus Vertretern des mit der Kriegspolitik der SPD-Führung unzufriedenen linken Flügels der SPD rekrutierenden neuen Partei.


=== Weimarer Republik (1919 bis 1933) ===
Nach der Rückkehr aus dem Krieg wurde Puchta 1919 Stadtverordneter in Plauen. Im selben Jahr wurde er dort leitender Redakteur der Volkszeitung. Danach arbeitete er als Redakteur in Berlin und seit 1924 wieder in Bayreuth.
Bei der Reichstagswahl im Juni 1920 wurde Puchta als Kandidat der USPD für den Wahlkreis 33 (Chemnitz-Zwickau) in den Reichstag gewählt, dem er zunächst bis zum Mai 1924 angehörte. Während dieser ersten Legislaturperiode der Weimarer Republik kehrte Puchta in die SPD zurück, deren Reichstagsfraktion er ab 1922 angehörte. Bei der Reichstagswahl im Mai 1928 wurde Puchta als Kandidat der SPD für den Wahlkreis 26 (Franken) in den Reichstag wiedergewählt, dem er ohne Unterbrechung bis zum Juni 1933 angehörte. Neben seiner Parlamentstätigkeit arbeitete Puchta an der linken Tageszeitung Fränkische Volkstribüne mit.


=== Zeit des Nationalsozialismus ===
Nach der nationalsozialistischen Machtergreifung im Frühjahr 1933 war Puchta einer der ersten SPD-Reichstagsabgeordneten, die von den Nationalsozialisten in „Schutzhaft“ genommen wurden. Obwohl sein Mandat noch in der Reichstagswahl im März 1933 bestätigt worden war, konnte Puchta an der Abstimmung über das Ermächtigungsgesetz nicht mehr teilnehmen, da er vier Tage nach der Wahl am 10. März festgenommen wurde. Nach seiner Entlassung aus dem Gefängnis Sankt Georgen in Bayreuth im Juli 1933 verdiente er seinen Lebensunterhalt als Lebensmittel- und Zeitschriftenhändler. In dieser Zeit hielt er Kontakt zu emigrierten Genossen im Ausland (Tschechien). Wegen dieser Widerstandstätigkeit wurde Friedrich Puchta im Juli 1935 erneut verhaftet und im Dezember 1935 vom Oberlandesgericht München wegen „Vorbereitung zum Hochverrat“ zu zweieinhalb Jahren Haft verurteilt, die er 1938 verbüßt hatte. Im August 1944 wurde Puchta im Rahmen der Aktion Gitter abermals in „Schutzhaft“ genommen und im KZ Dachau (Häftlings-Nr. 93.395) festgehalten. Nach der Häftlingskartei des Konzentrationslagers befand sich Puchta bis zur Befreiung in Dachau, nach anderen Angaben nahm er an einem der Todesmärsche teil, mit denen die SS versuchte, das KZ kurz vor Kriegsende zu evakuieren. Wenige Zeit später starb Puchta am 17. Mai 1945 an den Folgen der KZ-Haft in einem Krankenhaus in München-Schwabing.
Puchta wurde auf dem Bayreuther Stadtfriedhof bestattet. Sein Grabstein wurde aus dem Granit des ehemaligen Hakenkreuz-Denkmals am örtlichen Luitpoldplatz gefertigt.


== Gedenken ==

Die Friedrich-Puchta-Straße in Bayreuth erinnert an Puchtas Leben und Tätigkeit. In Berlin befindet sich eine Puchta gewidmete Gedenkplatte als Teil des Denkmals für 96 vom NS-Regime ermordete Reichstagsabgeordnete in der Scheidemannstraße.


== Schriften ==
 Unabhängige Sozialdemokratie oder Kommunistische Partei?, 1919.


== Literatur ==
 Friedrich. In: Franz Osterroth: Biographisches Lexikon des Sozialismus. Verstorbene Persönlichkeiten. Bd. 1. J. H. W. Dietz Nachf., Hannover 1960, S. 242–243.
 Martin Schumacher (Hrsg.): M.d.R. Die Reichstagsabgeordneten der Weimarer Republik in der Zeit des Nationalsozialismus. Politische Verfolgung, Emigration und Ausbürgerung, 1933–1945. Eine biographische Dokumentation. 3., erheblich erweiterte und überarbeitete Auflage. Droste, Düsseldorf 1994, ISBN 3-7700-5183-1. 


== Weblinks ==
 Literatur von und über Friedrich Puchta im Katalog der Deutschen Nationalbibliothek
 Friedrich Puchta in der Datenbank der Reichstagsabgeordneten


== Einzelnachweise ==