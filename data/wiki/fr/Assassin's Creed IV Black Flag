Assassin’s Creed IV Black Flag est un jeu vidéo d'action-aventure et d'infiltration développé par Ubisoft Québec et édité par la société Ubisoft. Il est sorti le 29 octobre 2013 sur PlayStation 3 et Xbox 360.
Les versions PlayStation 4 et Xbox One du jeu sortent lors des lancements respectifs des deux consoles. La version Wii U est décalée au 22 novembre 2013 en Europe. La version Windows sort le 19 novembre 2013 en Amérique du Nord, puis le 22 novembre en Europe.


== Trame ==


=== Contexte ===
Comme c'est le cas dans les précédents jeux de la série, l'histoire est divisée en deux moitiés entrelacées, comprenant une phase dans un monde futuriste, et une autre dans un cadre historique, les événements de chacun influençant l'autre.
De nos jours, le personnage du joueur est engagé par Abstergo Entertainment - une filiale d'Abstergo Industries - en tant que chercheur afin d'utiliser l'Animus et d'examiner les souvenirs de Desmond Miles et ses ancêtres. Bien qu'il ait déjà été établi que l'Animus ne peut être utilisé que pour afficher les souvenirs de ses ancêtres directs, la fin d’Assassin's Creed III implique que l'Animus fonctionne désormais de la même manière qu'un ordinateur. Face à cette nouvelle possibilité, Abstergo demande au participant de se concentrer spécifiquement sur les souvenirs d'Edward Kenway.


=== Résumé ===

Grâce aux échantillons récupérés sur le corps de Desmond Miles après sa mort, Abstergo a lancé un nouveau programme d'exploration des mémoires génétiques. Sous couvert du développement d'un nouveau concept de jeu vidéo interactif, il engage différentes personnes afin de découvrir les souvenirs passés des ancêtres de l'Assassin. Le personnage que le joueur contrôle, sans nom, est choisi pour étudier les mémoires d'Edward Kenway, le grand-père de Connor.
En 1715, Edward Kenway échoue sur une île après une bataille navale, seul survivant avec un dangereux assassin du nom de Duncan Walpole. Walpole, furieux d'avoir échoué à cause des pirates, tente de tuer Edward, mais échoue car son pistolet est hors d'usage. Edward parvient à rattraper et tuer Walpole. En le fouillant, il découvre une lettre d'un certain Laureano Torres, gouverneur de Cuba, promettant richesse à Walpole s'il lui apporte des cartes et un mystérieux cube en verre. Edward décide de se faire passer pour Walpole, enfile sa tenue, et gagne La Havane en embarquant sur un navire marchand d'un certain Stede Bonnet.
Une fois à La Havane, Edward se rend chez Torres, il y rencontre l'anglais Woodes Rogers et le français Julien Du Casse, deux autres partenaires de Torres. Torres se révèle en réalité être également le grand maître de l'ordre des Templiers dans les Caraïbes, ordre que rejoignent Rogers et Du Casse. Torres dévoile aux trois hommes qu'il est à la recherche d'un endroit appelé l'Observatoire, qui permettrait de localiser et suivre chaque personne sur Terre. Le lendemain, arrive via la flotte un mystérieux homme appelé le Sage, qui serait selon Torres la seule personne connaissant l'emplacement de l'Observatoire et pouvant ouvrir la porte. Après avoir échappé de justesse à une embuscade des Assassins, les ennemis jurés des Templiers, Torres offre sa récompense à Edward. Déçu de cette récompense qu'il juge faible, Edward, intrigué par cette histoire d'Observatoire, s'introduit dans les prisons de Torres afin de rencontrer seul le Sage. Il découvre alors que le Sage s'est échappé, et se fait ensuite maîtriser par El Tiburon, l'homme de main de Torres. Démasqué, Edward est envoyé avec la flotte du trésor. Grâce à une tempête, Edward parvient à s'échapper, en compagnie d'un pirate noir du nom d'Adéwalé.
Edward décide de garder le navire qu'il a volé et l'appelle "Le Jackdaw". Edward et Adéwalé se rendent à Nassau, fief de la confrérie pirate. Il retrouve alors de vieux amis : Benjamin Hornigold, Edward Thatch dit "Barbe Noire" et James Kidd, les fondateurs de la confrérie pirate. Edward tente de convaincre ses amis de se lancer à la recherche de l'Observatoire, mais ils refusent, ne croyant pas cette histoire. Barbe Noire fait par la suite remarquer que les défenses de Nassau sont trop faibles et décide alors de s'emparer d'un galion afin de renforcer les défenses. Edward et Barbe Noire suivent le galion jusqu'à une île où se trouve le Templier Julien Du Casse. Craignant que ce dernier ne le reconnaisse, Edward s'introduit dans l'île par l'arrière et tue Du Casse, avant de s'emparer du galion.
Le lendemain, James Kidd invite Edward à le retrouver à Tulum, un village situé au Yucatán. Edward s'y rend en pensant y trouver un trésor mais tombe sur le siège de la confrérie des Assassins, dont James Kidd fait en réalité partie. Il rencontre alors Ah Thabaï, le chef des Assassins, qui lui reproche d'avoir provoqué la trahison de Walpole en vendant les cartes des camps des Assassins à Torres. Edward se voit ainsi imposer d'aider les Assassins à repousser un assaut mené par un négrier du nom de Laurens Prins, ennemi des assassins. Grâce à l'aide d'Edward, les Assassins repoussent l'assaut. Ah Thabaï pardonne alors les erreurs d'Edward mais lui interdit de revenir. James Kidd, quant à lui, décide de rester à Tulum avec les Assassins.
De retour à Nassau, Edward rencontre les célèbres pirates Charles Vane et Jack Rackham, qui apprennent alors à Edward que le gouverneur Torres est en train de rassembler une grande quantité d'or dans un fort. Désireux de voler Torres, Edward attaque le fort, et apprend alors que cet or sert à payer le négrier Laurens Prins qui prétend détenir le Sage. Edward se rend alors à Kingston, lieu de vie de Prins, en utilisant Torres comme prisonnier et appât, pour trouver le Sage. Son plan tombe à l'eau quand James Kidd refait surface, désirant tuer Prins. Edward est obligé d'empêcher Kidd de commettre son acte, ce qui a pour conséquence de le faire repérer et de permettre à Torres de fuir. Furieux, Kidd ordonne à Edward de l'aider à tuer Prins une fois le Sage trouvé. Le soir venu, Edward et Kidd se retrouvent près du manoir de Prins. James révèle alors à ce moment-là qu'il est en réalité une femme du nom de Mary Read, qui se fait passer pour un homme. Edward et Mary infiltrent le manoir. Edward trouve et tue Prins, puis il trouve le Sage qui donne l'alerte, obligeant Edward et Mary à fuir le domaine.
De retour à Nassau, la nouvelle d'un pardon royal qui pourrait être accordé aux pirates provoque une forte dissension entre Barbe Noire et Benjamin Hornigold, qui commence à se lasser de voir Nassau dans la misère. Edward et Barbe Noire se lancent à la recherche de remèdes pour Nassau, en chemin, Edward retrouve Stede Bonnet, le marchant qui l'avait conduit à La Havane, et qui est désormais devenu un pirate. Edward et Barbe Noire parviennent à trouver des remèdes à Charlestown, c'est à ce moment que Barbe Noire décide d'annoncer à Edward qu'il est fatigué de la vie de pirate et se retire au nord.
Alors qu'Edward retourne une nouvelle fois à Nassau, le templier et désormais gouverneur Woodes Rogers arrive à Nassau accompagné du commodore Peter Chamberlain et d'un grand nombre de soldats. Rogers annonce alors officiellement que le pardon royal est offert à tout pirate. Benjamin Hornigold décide d'accepter le pardon royal, tandis qu'Edward et Charles Vane, refusant l'offre, préparent un plan pour fuir Nassau. Après avoir tué le commodore Chamberlain qui menaçait ce plan en voulant détruire les navires pirates, Edward et Charles Vane parviennent à fuir Nassau en chargeant un navire de poudre et en l'envoyant sur le blocus.
Edward se rend par la suite au Nord afin de faire revenir Barbe Noire. Ce dernier refuse, se sentant fini suite à la chute de Nassau. Il apprend cependant à Edward que le Sage est réapparu, sur un navire négrier appelé le "Princess". Les festivités qui suivent sont interrompues par l'armée anglaise, et le combat qui s'ensuit amène à la mort de Barbe Noire. Seul Edward parvient à fuir le massacre. Retrouvant Charles Vane quelque temps plus tard, les deux pirates se lancent à la recherche du "Princess". Après avoir appris que ce dernier transite par Kingston, Jack Rackham provoque une mutinerie à bord et abandonne Vane et Edward, s'emparant du "Jackdaw". Abandonnés sur une île, Charles Vane devient complètement fou et tente de tuer Edward. Ce dernier est obligé de le blesser et de laisser pour mort. Il parvient à s'enfuir de l'île en embarquant sur un navire marchand.
De retour à son repaire, il retrouve Adéwalé et Mary qui ont repris le "Jackdaw" à Rackham. Edward se rend alors à Kingston pour retrouver le "Princess", il tombe alors sur Woodes Rogers et Benjamin Hornigold, qui a trahi les pirates et rejoint les Templiers. Edward apprend en espionnant les Templiers que le "Princess" a été pris par des pirates à Principe, une île située sur les côtes de l'Afrique. Après avoir été repéré par Hornigold, il parvient à s'enfuir et se rend alors à Principe. Une fois à Principe, il retrouve alors le Sage, un homme désormais tourné vers la piraterie et connu sous le nom du capitaine Bartholomew Roberts. Afin de gagner sa confiance, Edward tue Burgess et Cockram, deux hommes de main d'Hornigold envoyés pour capturer Roberts. Ce dernier, reconnaissant, accepte alors de faire équipe avec Edward et lui confirme qu'il est le seul à connaître l'emplacement de l'observatoire.
Edward et Roberts parviennent quelque temps plus tard à voler un trésor des Templiers, composé des mystérieux cubes en verre que Edward a vendu à Torres, avec une goutte de sang de chaque Templier dans les cubes. Curieux de savoir à quoi servent les cubes, Edward supplie Roberts de lui montrer l'Observatoire ce que ce dernier accepte. En chemin, ils se rendent alors compte qu'ils sont suivis par Benjamin Horngold. Après avoir réussi à mettre le "Benjamin" hors d'état, l'obligeant à accoster près d'une île, Edward accoste l'île et tue Hornigold. De retour avec Roberts, les deux hommes arrivent enfin dans la salle de l'Observatoire, où Roberts ouvre la porte. Roberts montre alors à Edward comment marche l'appareil, tout simplement en plaçant le cube contenant le sang de la personne qu'on veut espionner dans l'appareil. Juste après, Roberts trahit Edward et l'enferme dans la salle de l'Observatoire. Edward parvient à s'échapper mais Adéwalé et son équipage se sont enfuis face à la menace des hommes de Roberts. Seul et blessé, Edward évanoui, Roberts le livre alors aux Anglais.
Edward passe les mois qui suivent en prison. Il assiste au procès de Mary, qui a été arrêtée en même temps qu'Anne Bonny, une pirate formée par Mary, ancienne serveuse à la taverne de Nassau et compagne de Rackham. Étant enceintes, elles échappent à la pendaison le temps de leurs grossesses. Pendant le procès, Rogers et Torres viennent voir Edward et tentent de le faire chanter pour savoir où se trouve l'Observatoire mais il refuse. Edward reste les mois qui suivent en prison, avant d'être finalement libéré par Ah Thabaï, qui promet de le sortir de là s'il l'aide à libérer Anne et Mary. En chemin, Edward apprend les morts de Rackham et Stede Bonnet, pendus, et retrouve Vane dans une prison, qui est devenu tellement fou qu'il ne reconnait même pas Edward. Edward et Ah Thabaï parviennent à libérer Anne, mais Mary décède des suites d'une maladie qu'elle a attrapée suite à son accouchement. Anéanti, Edward boit pour oublier, avant d'être réveillé par Adéwalé. Son fidèle ami et quartier-maître lui rend le « Jackdaw » mais décide de partir, invitant Edward à retourner à Tulum. Sur place, Edward apprend que Adéwalé a décidé de rejoindre les Assassins, et il décide de faire de même.
Maintenant officiellement Assassin et accepté par Ah Thabaï, Edward se met à traquer ses derniers ennemis. Accompagné par Anne, sa nouvelle seconde, il se rend à Kingston où se trouve Rogers. Il blesse sans tuer ce dernier. Promettant de récupérer l'appareil de l'observatoire à Roberts, Rogers révèle à Edward que Roberts a été aperçu à Principe. Edward se rend à Principe et le combat s'engage entre le « Jackdaw » et le « Royal Fortune ». Durant le combat, des frégates espagnoles et anglaises arrivent et attaquent le « Royal Fortune », ce qui permet à Edward d'aborder le navire et de tuer Roberts, récupérant ainsi l'appareil de l'observatoire. Utilisant l'appareil, il parvient à retrouver Torres à La Havane. Il pense le tuer mais il s'agissait de son second déguisé, et Edward se fait alors repérer par El Tiburon, l'homme de main de Torres. Après avoir combattu et tué El Tiburon, Edward comprend que Torres a trouvé l'Observatoire et s'y est rendu. Edward se rend alors son tour à l'Observatoire, où il retrouve et tue Torres. Ah Thabaï remet alors en place l'appareil et referme la porte de l'Observatoire.
Edward, Anne, Adéwalé et Ah Thabaï se retrouvent alors à 4 dans le repère d'Edward. Ce dernier a appris par courrier la mort de sa femme en Angleterre et l'existence d'une fille, Jennifer, qui doit arriver. Edward accueille alors sa fille et rentre avec elle en Angleterre, tournant le dos à la vie de piraterie.
Dans les locaux d'Abstergo, le joueur est contacté par John, un autre employé qui souhaite utiliser le joueur pour découvrir les secrets du bâtiment. Il lui donne accès à différents bureaux du bâtiment, ce qui permet au joueur d'espionner ses supérieurs, mais aussi de pirater les bases de données d'Abstergo, révélant ainsi certains documents cachés de la firme. Le joueur finit par accéder au sous-sol sécurisé du bâtiment qui renferme le noyau de l'Animus, où le joueur retrouve Junon. Elle explique alors que l'ouverture de son temple, bien que nécessaire, ne lui a pas permis d'achever son dessein car le monde n'est pas prêt. John révèle alors son plan : la réincarnation de Junon ; il espérait que Junon prendrait possession du corps du joueur, mais devant cet échec, il tente de tuer le joueur mais est tué par les agents d'Abstergo. Peu après, le joueur est contacté par Shaun Hastings et Rebecca Crane, qui lui demandent de continuer le piratage des ordinateurs d'Abstergo. La présence du Sage sur les lieux reste un mystère.


=== Personnages ===

Le personnage principal du jeu est Edward Kenway, un pirate britannique et membre de l'Ordre Assassin, le père de Haytham Kenway, et grand-père de Ratonhnhakéton (Connor), les deux personnages jouables de Assassin Creed III. Edward est amené à rencontrer des pirates célèbres.
Personnages fictifs :


== Système de jeu ==


=== Solo ===
Le jeu commence en 1715, en plein âge d'or de la piraterie, où il est possible de voyager dans les Caraïbes et de visiter leurs plus grandes villes dont La Havane, l'île de Tortuga, de Cozumel ainsi que Kingston et Nassau. Le joueur est amené à découvrir des temples mayas dans des jungles emplies de prédateurs. La plongée sous-marine fait son apparition, avec l'exploration d'épaves dans les tréfonds de la mer ou dans des grottes sous-marines à l'aide d'une cloche de plongée.
L'histoire commence par une bataille entre corsaires et espagnols. Le capitaine corsaire meurt en pleine bataille; Edward Kenway prend alors la barre et donne les ordres aux autres marins. Mais leur bateau coule et Edward nage en direction de l'île la plus proche. Il est le seul survivant avec un assassin. Il dit qu'il doit aller à la Havane, mais refuse l'aide d'Edward. Edward l'assassine et lit une lettre qui était dans sa poche. Cette lettre apprend que l'assassin s'appelle Duncan Walpole et qu'il doit donner la clé à Torres, en échange d'une somme d'argent. Edward décide donc d'enfiler le costume de Duncan Walpole, de se faire passer pour lui, d'aller à La Havane et récupérer l'argent. Mais les choses vont mal tourner...
Le système de déplacement rapide présent dans les précédents opus est toujours présent seulement si le lieu a été découvert. Cette fois le joueur peut naviguer librement avec le navire du capitaine Edward Kenway : le Jackdaw. Ce vaisseau peut être personnalisé, de l'armement à l'amélioration de la coque. Toutefois, le début du jeu implique la possession d'un petit navire, qui peut gagner en taille et en puissance suivant 5 niveaux d'amélioration.
Les batailles navales présentes dans le troisième opus le sont également dans cet épisode. Auparavant proposées en tant que missions secondaires, les batailles navales sont cette fois-ci présentes tout au long de l'aventure, y compris durant les missions principales. Elles sont aussi plus variées, et certains lieux de la carte ne sont pas accessibles, car gardés par des navires beaucoup plus coriaces que le Jackdaw. Il est primordial d'améliorer son navire et de recruter des membres d'équipage pour pouvoir y accéder. Dans Assassin's Creed III, une des armes caractéristiques de Connor était un arc, dans cet opus est présente entre autres une sarbacane.


=== Multijoueur ===
Le multijoueur comporte plusieurs modes compétitifs. Les modes de jeux sont accompagnés d'une sélection de cartes et de personnages. Contrairement à la campagne solo, le multijoueur ne comporte pas de batailles navales et se déroule sur des îles.


==== Personnages ====
Le multijoueur de AC4 reprend les mêmes bases que celui d'AC3 en matière de personnages (certains sont présents dans les deux opus) : une dizaine de personnages ayant eu des relations filiales, intimes ou autres au cours de leur vie ; contrairement à AC Brotherhood et Revelations, les personnages ne sont pas tous des Templiers, à l'exemple du « Rodeur Nocturne » qui est un Assassin ou du « Boucanier », qui, lui, est neutre. Voici une liste des personnages présents dans cet opus :
Le Baroudeur, un jeune homme qui, après avoir parcouru le monde, vient prendre cette fois une (nouvelle) fausse identité de récolteur de café ; Abstergo laisserait entendre qu'il serait lié à des affaires douteuses, voire qu'il soit un tueur à gages ;
Le Dandy, un homme qui serait né en Angleterre de parents roturiers mais avec beaucoup d'ambition. Après être passé par la Comédie-Française où il fréquenta les plus grands et prit surement goût aux choses luxueuses, il embarqua sur un navire pour les Caraïbes et hérita, peu de temps après, de la plantation d'un homme ; il prit alors un autre nom à particule et fit croire qu'il venait de France, pour accroître son prestige.


== Développement ==
Au début de mois de février 2013, lors de son appel financier trimestriel aux investisseurs, le PDG d'Ubisoft, Yves Guillemot, confirme que le prochain Assassin's Creed, serait publié peu de temps avant avril 2014 et qu'il comportera un nouveau héros, une nouvelle période de temps, un nouveau thème ainsi qu'une nouvelle équipe de développement. Le 28 février 2013 Ubisoft publie plusieurs images promotionnelles ainsi que la couverture du jeu en question, le titre du jeu est dévoilé peu après, la couverture présente Edward Kenway sur son navire armé d'un sabre argenté et d'un pistolet, avec en arrière-plan un marin et un drapeau représentant l’icône de la série et celle des assassins ainsi que l'Ordre pirate-assassin d'Edward représenté par une tête de mort. Une fuite d'Ubisoft annonce que le jeu est prévu pour les consoles de huitième génération telles la Xbox One, la PlayStation 4 et la Wii U.
Peu de temps après, la bande annonce officielle du jeu est publiée, le 4 mars (à l'origine il y a une fuite contenant la bande annonce le 2 mars, mais rapidement retirée par Ubisoft). De plus une bande annonce Xbox 360 le 4 mars a été aussi annoncé. Le jeu est en développement depuis mi-2011 chez Ubisoft Montréal, la principale équipe à développer le jeu est celle du précédent opus, Assassin's Creed III, cependant plusieurs autres studios collaborent au projet, tels Ubisoft Annecy, Bucarest, Kiev, Montpellier, Québec, Singapore et Sofia.


== Doublage ==


=== Doublage anglophone ===

Matt Ryan : Edward Kenway
Olivia Morgan: James Kidd/Mary Read
Ralph Ineson: Charles Vane
Mark Bonnar: Edward "Blackbeard" Thatch
Tristan D Lalla: Adéwalé
Luisa Guerreiro: Caroline Scott-Kenway
Sarah Greene: Anne Bonny
James Bachman: Stede Bonnet
Cristina Rosato: Melanie Lemay
Vincent Hoss-Desmarais: Olivier Garneau
Danny Wallace: Shaun Hastings
Eliza Jane Schneider: Rebecca Crane
O T Fagbenle: Jack Rackham
Angela Galuppo: Jennifer Scott
Olivier Milburn: Bartolomew Roberts
Nadia Verrucci: Junon
Kwasi Songui: Anto
Milton Lopes: Ah Tabai
Alex Ivanovici: Julien du Casse
Ed Stoppard: Benjamin Hornigold
Shaun Dingwall: Woodes Rogers
Conrad Pla: Laureano de Torres


=== Doublage francophone ===
Jean-François Beaupré : Edward James Kenway
Didier Lucien : Adéwalé
Sylvain Hétu : Edward « Barbe Noire » Thach
Tristan Harvey : Bartholomew Roberts
Olivier Visentin : Stede Bonnet
Marc Bellier : Laureano de Torres y Ayala
François Godin : Julien du Casse
Nicolas Charbonneaux-Collombet : Desmond Miles
Benoit Rousseau : Peter Chamberlaine et Emmett Scott


== Contenu additionnel ==


=== Le prix de la Liberté ===
Le DLC officiel d'Assassin's Creed IV: Black Flag fait découvrir une nouvelle facette d'un personnage évoqué dans le jeu basique, Adéwalé, grand compagnon d'Edward Kenway. C'est la première fois qu'une vraie extension change complètement de héros. Une nouvelle zone des Caraïbes nous est aussi donnée à découvrir : Port-au-Prince et ses alentours.
L'histoire se déroule des années après qu'Adéwalé a quitté le Jackdaw, navire d'Edward Kenway. Adéwalé a trouvé auprès des Assassins et leur mentor Ah Tabai un vrai réconfort, un idéal à suivre, une raison de se battre ; ses années de marin l'ayant endurci et lui ayant appris beaucoup de choses, son entraînement est rapide et fait de lui un brillant Assassin. Mais lors d'une mission de routine où, avec son navire, il doit intercepter un convoi templier et récupérer un colis important aux mains de ses ennemis, il s'aperçoit que la flotte adverse est beaucoup plus nombreuse que ses estimations et, après avoir récupéré le colis, fuit vers les côtes malgré la terrible tempête qui éclate. Adéwalé, après une course-poursuite, est éjecté de son navire par un rouleau pendant que son navire sombre et son équipage meurt.


=== Aveline ===
Ce contenu nous fait découvrir un autre pan de la vie d'Aveline de Grandpré, Assassin déjà évoqué dans Assassin's Creed Libération. Cette fois, elle est envoyée par le chef de la Confrérie, Connor, petit-fils d'Edward Kenway, afin de retrouver une sympathisante aux mains des Templiers. Les mécaniques de jeu sont reprises d'Assassin's Creed III. C'est un contenu exclusif à la PlayStation 3, la PlayStation 4 et la version PC.


== Sortie ==
Assassin Creed IV: Black Flag sort dans un premier temps sur PlayStation 3, Xbox 360 le 29 octobre, puis le 22 novembre sur PlayStation 4, Xbox One, PC et Wii U en Europe. Ubisoft a conclu un nouveau partenariat avec Sony pour apporter 60 minutes de gameplay supplémentaire et exclusif aux versions PlayStation 3 et PlayStation 4.
Une édition spéciale est également sortie avec du contenu additionnel : « Secrets sacrifiés » (des trésors de Francis Drake sont disponibles sur l'île des Sacrifices) et « L'héritage du Captaine Kenway » (armes et costumes supplémentaires).
Le 27 mars 2014 est sorti sur PC, PS4 et Xbox One une édition intitulée Jackdaw Edition. Celle-ci comprend un code permettant de télécharger l'ensemble des contenus additionnels à l'exception du DLC d'Aveline.


== Manga ==
Une série dérivée manga intitulée Assassin's Creed Awakening a été prépublié dans le magazine Jump Kai entre le 10 août 2013 et le 7 juillet 2014. Celle-ci est scénarisée par Yano Takashi et dessinée par Oiwa Kenji. Le premier volume est publié par Shueisha le 10 mars 2014. La version française est éditée par Ki-oon depuis juin 2014.


== Accueil ==


=== Critique ===

Assassin's Creed IV: Black Flag a reçu de bonnes critiques, saluant la richesse de l'univers et l'ambiance de piraterie retranscrite, mais pointant du doigt des défauts de gameplay hérités des opus précédents et un scénario secondaire et sans vraie surprise, ayant en grande partie filtré sur Internet avant la sortie.


=== Controverse avec PETA ===
L'organisation PETA a dénoncé Assassin's Creed IV: Black Flag pour avoir intégré la chasse à la baleine, accusant le jeu de « glorifier » cette activité,.
Ubisoft s'est défendu en disant que l'inclusion de cet élément n'est là que pour représenter des événements de cette période historique, sans volonté de la montrer sous un beau jour, pas plus que la piraterie.


== Autour du jeu ==
En novembre 2013, la société de jeux vidéo Ubisoft a financé l'exhumation des restes du corsaire espagnol Amaro Pargo afin de reconstituer son visage pour sa possible apparition dans Assassin's Creed IV: Black Flag. Pour la première fois dans l'histoire, une entreprise de l'industrie du jeu vidéo exhume les restes d'un personnage historique dans une campagne qui a attiré l'attention des médias nationaux et internationaux. Cette exhumation a permis d'importantes découvertes sur la physionomie de ce corsaire.


== Notes et références ==


== Lien externe ==
(fr) Site officiel

 Portail du jeu vidéo
 Portail d'Ubisoft
 Portail de la piraterie
 Portail de la science-fiction
 Portail de la Caraïbe