La planétologie est la science de l'étude des planètes. Cette discipline recouvre de nombreuses branches de la science : son domaine d'étude s'étend des grains microscopiques jusqu'aux planètes géantes gazeuses.
La recherche se fait généralement en combinant les observations faites depuis le sol à l'aide de télescopes, celles effectuées par des engins spatiaux ainsi que des travaux expérimentaux ou théoriques faits sur Terre. La planétologie partage beaucoup de sujets d'intérêt et d'outils méthodologiques avec les sciences de la Terre qui d'une certaine manière font elles-mêmes partie intégrante de la planétologie.

La planétologie comparée est la science comparant les caractéristiques des différentes planètes. Elle regroupe de multiples sous-disciplines telles : la géochimie, la magnétohydrodynamique, la géologie, etc. Un de ses principaux objectifs est de comparer la Terre aux autres planètes telluriques comme Mars et Vénus en s’intéressant aux processus de différenciation, à l'évolution tectonique et géodynamique, à l'origine et à l'évolution des éléments volatils et des atmosphères.
D'un point vue purement sémantique, lorsque la discipline se concentre sur un corps céleste en particulier, un terme spécialisé peut être utilisé, comme indiqué ci-dessous, bien qu'en pratique seul géologie soit réellement usité :
Longtemps la planétologie est restée confinée à l'étude du Système solaire, seul système planétaire connu jusqu'en 1995. Avec la découverte de la première planète extrasolaire « ordinaire » en 1995 par Didier Queloz et Michel Mayor autour de l'étoile 51 Pegasi, la planétologie a connu un essor et une diversification considérables, les nouveaux systèmes planétaires découverts différant radicalement du Système solaire. Lorsqu'elle compare les planètes situées en dehors du système solaire, cette science prend parfois le nom d'exoplanétologie : cette branche de la discipline cherche à classer les planètes et à en déterminer les conditions d'apparition, voire les critères biophiles (qui sont propices à la vie).


== Notes et références ==


== Voir aussi ==


=== Bibliographie ===
Sylvain Bouley, Lionel Bret, Petit atlas des étoiles et planètes: comprendre l'univers en 40 repères, Delachaux et Niestlé, 2009, 22 p.


=== Articles connexes ===
Exoplanète
Planète
Système solaire
 Portail de l’astronomie
 Portail des sciences de la Terre et de l’Univers