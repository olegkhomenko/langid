Cervus elaphus corsicanus

Le cerf élaphe de Corse (Cervus elaphus corsicanus) ou cerf rouge de Corse ou cerf sarde est une sous-espèce du Cerf élaphe (Cervus elaphus), mammifère artiodactyle ruminant de la famille des cervidés. Il a été observé pour la première fois en Corse, d'où son nom. Cependant, cet animal vit aussi en Sardaigne.


== Introduction préhistorique et ré-introduction ==
Absent du massif corso-sarde durant tout le Pléistocène et la première moitié de l'Holocène, le cerf élaphe a été introduit en corse à l'époque romaine voire un peu avant, soit à partir de populations italiennes continentales de petite taille, soit à partir de celles de Sardaigne, introduites sur cette dernière île depuis la fin du Néolithique au moins. La petite taille des sujets insulaires a très tôt intrigué les naturalistes, comme en témoignent ces propos de Buffon en 1756 : "Et ce qui m'a convaincu que la grandeur et la taille des cerfs en général dépendaient absolument de la quantité et de la qualité de la nourriture, c'est qu'en ayant fait élever un cerf de Corse chez moi, et l'ayant nourri largement pendant quatre ans, il était à cet âge beaucoup plus haut, plus gros, plus étoffé que les plus vieux cerfs de mes bois, qui cependant sont de belle taille."

Les populations de Corse de cerf élaphe avaient disparu à la fin des années 1960, le dernier individu ayant été abattu par un braconnier. Une nouvelle introduction de l'espèce sur l'île a été entreprise dans le parc naturel régional de Corse par J.Leoni à partir de population de Sardaigne en 1985 en partenariat avec la région sarde (Dr Enea Beccu). Conservés dans trois enclos, les premiers individus ont été mis en liberté complète en 1998 sur la commune de Quenza. D'autres lâchers ont suivi dans les communes de Chisa, Santo-Pietro-di-Venaco, Moltifao, Guagno, Letia, Soccia et Castifao. Actuellement, on estime sa population corse à quelques 800 individus.
Sa population sarde est estimée à plus de 6 000, principalement concentrés sur le Mont Arcosu, les Montagnes du Sulcis, les Montagnes du Sarrabus et le territoire d'Arbus.
Ceci ne permet pas pour autant de relâcher l'attention sur cette espèce car les dangers qui la menacent restent d'actualité : braconnage, feux de forêts.


== Alimentation ==


== Statut et protection ==
Le cerf élaphe corse a été inscrit en 2000 sur la liste rouge des espèces menacées de l'UICN.
En Italie, l'espèce est également enregistrée sur le registre des espèces particulièrement protégées, au niveau national (art. 2 L. 157/92) et régional (art 5 L.R. 23/98).


== Notes et références ==


=== Références taxinomiques ===
Référence INPN : Cervus elaphus corsicanus Erxleben, 1777 (+ statut + description) (fr)
 Portail des mammifères