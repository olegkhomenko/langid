Cause-de-Clérans est une commune française située dans le département de la Dordogne, en région Nouvelle-Aquitaine.


== Géographie ==
Au sud du département de la Dordogne, en Périgord pourpre, la commune de Cause-de-Clérans, située dans l'aire urbaine de Bergerac, est arrosée par le Clérans, petit ruisseau affluent de la Dordogne.
L'altitude minimale, 47 mètres, se situe au sud-ouest, à l'ouest du lieu-dit Bénéventie, là où le Clérans quitte la commune pour entrer sur celle de Mouleydier. L'altitude maximale avec 156 mètres se trouve au nord-est, en bordure de la commune de Pressignac-Vicq, à l'est du lieu-dit Crocherie.
Le bourg de Cause-de-Clérans, longé par la route départementale 36 qui traverse la commune du nord-est au sud-ouest, se situe en distances orthodromiques, six kilomètres à l'ouest-nord-ouest de Lalinde et quinze kilomètres à l'est de Bergerac.


=== Communes limitrophes ===


== Toponymie ==
En occitan, la commune porte le nom de Cause de Clarenç.


== Histoire ==
Les deux bourgs médiévaux de Cause et de Clérans apparaissent dans les écrits pour la première fois au XIIe siècle sous les formes latines de Cauzia et de Clarentium.


== Politique et administration ==


== Population et société ==


=== Démographie ===

L'évolution du nombre d'habitants est connue à travers les recensements de la population effectués dans la commune depuis 1793. À partir de 2006, les populations légales des communes sont publiées annuellement par l'Insee. Le recensement repose désormais sur une collecte d'information annuelle, concernant successivement tous les territoires communaux au cours d'une période de cinq ans. Pour les communes de moins de 10 000 habitants, une enquête de recensement portant sur toute la population est réalisée tous les cinq ans, les populations légales des années intermédiaires étant quant à elles estimées par interpolation ou extrapolation. Pour la commune, le premier recensement exhaustif entrant dans le cadre du nouveau dispositif a été réalisé en 2007.
En 2015, la commune comptait 346 habitants, en augmentation de 6,13 % par rapport à 2010 (Dordogne : +0,31 %, France hors Mayotte : +2,44 %).


=== Enseignement ===
En 2012, Cause-de-Clérans n'a plus d'école et est organisée en regroupement pédagogique intercommunal (RPI) avec les communes de Baneuil et Couze-et-Saint-Front au niveau des classes de maternelle et de primaire.


== Économie ==


=== Emploi ===
En 2012, parmi la population communale comprise entre 15 et 64 ans, les actifs représentent 147 personnes, soit 44,0 % de la population municipale. Le nombre de chômeurs (dix-sept) a augmenté par rapport à 2007 (onze) et le taux de chômage de cette population active s'établit à 11,6 %.


=== Établissements ===
Au 31 décembre 2013, la commune compte trente-trois établissements, dont treize au niveau des commerces, transports ou services, neuf dans l'agriculture, la sylviculture ou la pêche, cinq dans la construction, quatre dans l'industrie, et deux relatifs au secteur administratif, à l'enseignement, à la santé ou à l'action sociale.


== Culture locale et patrimoine ==


=== Lieux et monuments ===
Château de Clérans, XIIe, XIIIe et XIVe siècles, inscrit aux monuments historiques en 1948 pour son donjon puis en 2007 pour le reste du château
Église Notre-Dame-de-l'Assomption de Cause du XIIe siècle, inscrite en 1948


=== Patrimoine naturel ===
Le coteau de Peymourel est un site naturel remarquable géré par le Conservatoire d'espaces naturels Aquitaine.
À l'ouest, plus du tiers du territoire communal se situe en forêt de Liorac, une zone naturelle d'intérêt écologique, faunistique et floristique (ZNIEFF) de type II, refuge de la grande faune,.


=== Personnalités liées à la commune ===


=== Héraldique ===


== Voir aussi ==


=== Articles connexes ===
Dordogne (département)
Périgord
Liste des communes de la Dordogne
Liste des châteaux et demeures de la Dordogne


=== Liens externes ===

Cause-de-Clérans sur le site de l'Institut géographique national (archive)
Château de Clérans sur richesheures.net


== Notes et références ==


=== Notes ===


=== Références ===

 Portail de la Dordogne
 Portail des communes de France