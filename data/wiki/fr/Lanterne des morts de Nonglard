La lanterne des morts de Nonglard est une lanterne des morts située à Nonglard, en France.


== Localisation ==
Ce curieux édifice mieux connu sous l’appellation "Le monument" est situé au Chef-lieu, au croisement des chemins conduisant vers les hameaux du village et à moins de 50 mètres de l’Église.


== Description ==
C’est une tour cylindrique en maçonnerie d’un diamètre de 1,60 m, de 4 m de hauteur, coiffée d’une calotte avec une croix en fer forgé aux signes franc-maçonniques. À la base de la coupole, quatre ouvertures (lumignons) sont destinées à contenir des lumières orientées en fonction des points cardinaux. À mi-hauteur, une niche en plan inclinée peut recevoir un livre de prière. Son architecture dépouillée, sa forme et la courbe de son dôme ont permis de situer sa construction dans la première partie du XVIIe siècle et il faut aller jusqu’en Limousin pour en retrouver la réplique. Sa présence sur ce site trouve son origine dans le rite important du culte des morts qui consistait à entretenir des lumières sur les sépultures. Les premiers chrétiens, comme les païens, mettaient des lampes sur les tombeaux (nous allumons encore des cierges autour des morts) ; des porte cierges et même des lanternes étaient disposées sur les tombes du Moyen Âge et la lanterne des morts était un fanal brûlant en l’honneur de tous ceux qui y reposent et éclairant les veillées que l’on venait faire par dévotion. Dans la pensée des gens superstitieux, la lumière chassait les mauvais esprits, mais la pensée de l’église semble d’avoir été d’attirer l’attention des fidèles et de leur suggérer de prier pour les morts ; c’est pour cette raison que la croix s’élevait au-dessus de l’enclos du cimetière et que, quand la nuit rendait invisible, le fanal s’allumait.


== Historique ==
Généralement, les lanternes des morts étaient édifiées dans l’enceinte même des cimetières qu’elles éclairaient durant toute la nuit. Mais celle de Nonglard présente une originalité car elle se dresse à un croisement de chemins. Lorsqu’on observe sa situation sur la mappe sarde, on s’aperçoit que l’espace compris entre l’église et la lanterne, actuellement place communale avec la mairie, était une propriété privée. Le Maître des lieux est alors, en 1730, Pierre Pottier. Son grand-père, Jacques Pottier, était chirurgien à Annecy où il épousa en 1650 Charlotte Leya dont le père, Jehan, bourgeois d’Annecy, était alors propriétaire de ce terroir. C’est donc à partir de 1650 que les Pottier mirent pied à Nonglard grâce à ce mariage après avoir quitté l’Anjou, dont ils étaient originaires, pour se réfugier en Savoie, pour cause de guerre de religions.
Gilbert Viviant, membre de l’académie florimontane, qui a consacré de longues années de recherches pour éclairer l’étonnante lanterne de Nonglard, apporte une explication plausible : "À la fin de 1669 et au début de l’année suivante, des épidémies ayant à nouveau frappé la commune et ayant causé 25 décès dont onze enfants (peut-être à cause de la variole), Jacques Pottier fit alors édifier la lanterne des morts au bord du chemin jouxtant l’entrée de sa propriété comme il en existait dans les provinces d’origine de son père et de son grand-père. C’est ainsi que les lumières disposées dans cette lanterne au centre du village indiquaient qu’on était en période de contagion et faisaient partie des moyens de limiter l’extension d’une épidémie."
Ce monument de "protection locale" qui n’est plus éclairé que par la lumière du jour est inscrit à l’inventaire des monuments historiques depuis 1964. Une restauration a été réalisée en 1996 pour mettre en valeur cet étrange monument qui sert à éloigner les esprits maléfiques et attirer notre curiosité d’aujourd’hui.


== Annexes ==


=== Articles connexes ===
Liste des monuments historiques de la Haute-Savoie


=== Références ===

 Portail des monuments historiques français
 Portail de la Savoie