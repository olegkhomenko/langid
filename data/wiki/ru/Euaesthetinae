Euaesthetinae  (лат.) — подсемейство жуков-стафилинид.


== Описание ==
Мелкого размера жуки (около 2 мм). Голова большая и плоская, глаза мелкие, округлые, расположены у заднего края головы. Усики булавовидные. Переднеспинка небольшая, часто с длинным бороздам в середине. Древнейшие ископаемые представители обнаружены в раннем Мелу в Ливанском янтаре.
Около 15 родов известны из Южного полушария (Австралия, Новая Зеландия, Южная Африка, юг Южной Америки). Всего включает 850 видов и 30 родов.

 Euaesthetus bipunctatus Lj. — Западная Палеарктика, в том числе Россия.


== Систематика ==
 Триба Alzadaesthetini Scheerpeltz, 1974
 Род Alzadaesthetus Kistner, 1961
 Триба Austroesthetini Cameron, 1944
 Austroesthetus — Chilioesthetus — Kiwiaesthetus — Mesoaesthetus — Nothoesthetus — Tasmanosthetus
 Триба Euaesthetini Thomson, 1859 (=Tamotini)
 Coiffaitia — Ctenomastax — Edaphus — Euaesthetotyphlus — Euaesthetus — Macroturellus — Neocoiffaitia — Octavius — Phaenoctavius — Protopristus — Schatzmayrina — Tamotus
 Триба Fenderiini Scheerpeltz, 1974
 Fenderia Hatch, 1957
 Stictocranius
 Триба Nordenskioldiini Bernhauer & Schubert, 1911
 Edaphosoma
 Nordenskioldia Sahlberg, 1880
 †Libanoeuaesthetus Lefebvre, Vincent, Azar & Nel, 2005
 †Libanoeuaesthetus pentatarsus Lefebvre, Vincent, Azar & Nel, 2005
 Триба Stenaesthetini Bernhauer & Schubert, 1911
 Agnosthaetus
 Gerhardia
 Stenaesthetus Sharp, 1874
 Tyrannomastax


== Примечания ==


== Литература ==
 Clarke, D.J.; Chatzimanolis, S. 2009: Antiquity and long-term morphological stasis in a group of rove beetles (Coleoptera: Staphylinidae): description of the oldest Octavius species from Cretaceous Burmese amber and a review of the «Euaesthetine subgroup» fossil record. // Cretaceous research, 30: 1426—1434.
 Clarke, D.J.; Grebennikov, V.V. 2009: Monophyly of Euaesthetinae (Coleoptera: Staphylinidae): phylogenetic evidence from adults and larvae, review of austral genera, and new larval descriptions. // Systematic Entomology, 34: 346—397. doi: 10.1111/j.1365-3113.2009.00472.x
 Herman, L.H. 2001: Catalog of the Staphylinidae (Insecta, Coleoptera): 1758 to the end of the second millennium. IV. Staphylinine group (part 1) Euaesthetinae, Leptotyphlinae, Megalopsidiinae, Oxyporinae, Pseudopsinae, Solieriinae, Steninae. // Bulletin of the American Museum of Natural History, (265): 1807—1220.
 Lefebvre, F.; Vincent, B.; Azar, D.; Nel, A. 2005: The oldest beetle of the Euaesthetinae (Staphylinidae) from Early Cretaceous Lebanese amber. // Cretaceous research, 26: 207—211.


== Ссылки ==
 Подсемейство Euaesthetinae — атлас стафилинид (Staphylinidae) России и сопредельных стран
 Euaesthetinae — bugguide.net.  (англ.) (Проверено 23 января 2011)
 eol.org