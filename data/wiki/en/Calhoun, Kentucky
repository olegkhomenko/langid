Calhoun is a home rule-class city in McLean County, Kentucky, United States. The population was 763 at the 2010 census. It is the county seat of McLean County. It is included in the Owensboro, Kentucky Metropolitan Statistical Area.


== Geography ==
Calhoun is located at 37°32′19″N 87°15′34″W (37.538633, -87.259414), on the Green River, the longest river entirely in the Commonwealth of Kentucky, and is home to the Army Corps of Engineers' Lock and Dam #2.
According to the United States Census Bureau, the city has a total area of 0.7 square miles (1.8 km2), of which 0.7 square miles (1.8 km2) is land and 0.04 square miles (0.10 km2) (2.86%) is water.


== History ==
Present-day Calhoun was first known as Rhoadsville after the German-born Pennsylvanian Captain Henry Rhoads (1739–1809), who laid out the town from 1784 to 1785 near the Long Falls of the Green River. His brother Solomon then erected a fort to protect the settlers and the transit around the falls.
Around the time John Hanley acquired Rhoad's lands in a 1787 lawsuit, the site was renamed and was alternately known as Fort Vienna and Vienna Station. The post office established in 1849, however, was called Calhoon after Rep. John Calhoon of Kentucky and the town was chartered by the state assembly in 1852 under this new name. At some later point, presumably out of confusion with the more famous Senator and Vice President John Calhoun of South Carolina, the spelling of the town was changed.
In 1854, it beat out the settlement of Rumsey on the other side of the river to become the seat of the newly formed McLean County.


== Demographics ==
2000 Census
As of the census of 2000, there were 836 people, 357 households, and 210 families residing in the city. The population density was 1,231.1 people per square mile (474.7/km²). There were 395 housing units at an average density of 581.7 per square mile (224.3/km²). The racial makeup of the city was 98.80% White, 0.60% African American, 0.12% Native American, 0.24% from other races, and 0.24% from two or more races. Hispanic or Latino of any race were 0.60% of the population.
There were 357 households out of which 24.4% had children under the age of 18 living with them, 45.1% were married couples living together, 11.2% had a female householder with no husband present, and 40.9% were non-families. 38.1% of all households were made up of individuals and 18.5% had someone living alone who was 65 years of age or older. The average household size was 2.11 and the average family size was 2.77.
In the city, the population was spread out with 18.8% under the age of 18, 6.8% from 18 to 24, 23.4% from 25 to 44, 22.0% from 45 to 64, and 28.9% who were 65 years of age or older. The median age was 46 years. For every 100 females there were 73.8 males. For every 100 females age 18 and over, there were 68.9 males.
The median income for a household in the city was $23,438, and the median income for a family was $32,386. Males had a median income of $31,500 versus $16,719 for females. The per capita income for the city was $22,520. About 11.8% of families and 19.1% of the population were below the poverty line, including 21.1% of those under age 18 and 20.4% of those age 65 or over.
2010 Census
The 2010 census  reflected a decline in the city's population to 763. These lived in 317 households, with 184 of those being families. Seventy-four families had children under the age of 18. The average household size was 2.15, and the average family size was 2.85. In 122 households, there was one individual.
Racially, 754 of the 763 were white, four were Native American and five were of more than one race. Two white individuals identified as Hispanic or Latino, with origins in Mexico.
The city's median age was 46.4 years. Males were 44.2, while females were 49.3, reflecting greater longevity. For age 85 and above, there were 19 males and 38 females.


== Notable person ==
Glover H. Cary - former United States Representative


== Climate ==
The climate in this area is characterized by hot, humid summers and generally mild to cool winters. According to the Köppen Climate Classification system, Calhoun has a humid subtropical climate, abbreviated "Cfa" on climate maps.


== References ==


== Further reading ==
Prichard, Alvin L. (April 1929). "The Beginning of Old Vienna, Now Calhoun in McLean County". Filson Club History Quarterly. 3 (3). Retrieved 2011-11-11.