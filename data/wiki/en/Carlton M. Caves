Carlton "Carl" Morris Caves is an American physicist. He is currently a Distinguished Professor in Physics and Astronomy at the University of New Mexico. Caves works in the areas of physics of information; information, entropy, and complexity; quantum information theory; quantum chaos, quantum optics; the theory of non-classical light; the theory of quantum noise; and the quantum theory of measurement.


== Background and education ==
Caves was born in Muskogee, Oklahoma, on October 24, 1950, the son of Morris and Mary Caves. After attending public schools in Muskogee, Caves attended Rice University as an undergraduate, receiving a BA in physics and mathematics in 1972. He then was a graduate student at the California Institute of Technology, where he received a PhD in physics in 1979 under Kip Thorne with dissertation Theoretical Investigations of Experimental Gravitation.


== Career ==
After receiving his PhD, Caves continued at Caltech as a Research Fellow in Physics (1979–81) and then as Senior Research Fellow in Theoretical Physics (1982–87). During 1987–92 he was Associate Professor of Electrical Engineering (and Physics from 1989) at the University of Southern California. He moved to Albuquerque in 1992 to become a Professor of Physics and Astronomy at the University of New Mexico (UNM). In 2006 he was promoted to Distinguished Professor, UNM’s highest faculty rank. In 2009 he was appointed the inaugural Director of the Center for Quantum Information and Control (CQuIC), an interdisciplinary center at UNM and the University of Arizona, which investigates and develops a new generation of technologies for controlling the behavior of quantum systems. Caves is best known for his proposal in 1981 that squeezed light injected into the vacuum port of an interferometer can improve the interferometer’s sensitivity for detecting small phase changes. This proposal prompted thirty years of technology development to design squeezed-light sources that can improve the exquisite sensitivity achieved by the very large interferometers that have been constructed to detect gravitational waves from astrophysical events. The squeezed-light technique has been demonstrated in the GEO600 and LIGO gravitational-wave detectors.
Caves has also made contributions to the theory of continuous measurements in quantum mechanics, participated in initial work on what is now called Quantum Bayesianism, worked on a proposal for doing two-qubit quantum gates on neutral atoms trapped in an optical lattice, helped to clarify the role of quantum entanglement in Nuclear Magnetic Resonance (NMR) simulations of quantum computation, and explored the role of nonclassical correlations outside of quantum entanglement as the resource that powers quantum computation.
Caves is the author of over 140 scientific papers on these and other topics. His present research is concentrated on quantum metrology, quantum control, and quantum information science.
Caves has also made critical comments on J. Richard Gott’s use of a temporal Copernican principle to predict the future duration of a phenomenon based only knowing the phenomenon’s present age.


== Family and other interests ==
Caves married Karen L. Kahn on 3 June 1984. They reside in Albuquerque, where Kahn is a partner at the law firm of Modrall Sperling. They have two children: Jeremy Caves, a PhD student in Environmental Earth Systems Science at Stanford University, and Eleanor Caves, a PhD student in Biology at Duke University. In addition to his scientific interests, Caves is an avid bird-watcher and an ardent environmentalist. He is a member of the Board of Audubon New Mexico and Chair of the Board’s Conservation Committee.


== Awards and honors ==
1972–75 — National Science Foundation (NSF) Predoctoral Fellow
1976-77 — Richard P. Feynman Fellowship (as a PhD student)
1976 — inaugural recipient of the Öcsi Bácsi “Deeply Dedicated to Physics” Award
1990 — (with Daniel Walls) Einstein Prize for Laser Science from the Society for Optical and Quantum Electronics
2004 — elected a Fellow of the American Physical Society
2008 — elected a Fellow of the American Association for the Advancement of Science
2011 — Max Born Award from the Optical Society of America


== See also ==
Quantum Aspects of Life


== References ==


== External links ==
Caves's homepage
Caves's CV
Center for Quantum Information and Control, UNM
Caves's New Mexico Diaries
Caves's father
Caves's mother
Caves's math genealogy
Publications of Carlton Morris Caves in the database SPIRES
arXiv.org preprints for C. Caves
search on author Carlton Caves from Google Scholar