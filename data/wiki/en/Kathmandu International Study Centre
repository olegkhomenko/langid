
== HOME ==
KISC is an internationally accredited Christian school in Kathmandu, Nepal.
It was founded in 1987 by United Mission to Nepal (UMN) and International Nepal Fellowship (INF) to provide education to expatriate mission families in Nepal. It is currently owned by Human Development and Community Services (HDCS).
At KISC, about 70% of the students are from mission families and the other 30% are students seeking an international education. It is a culturally diverse community with over 30 different nationalities represented by students and staff.
The school’s mission statement is: “To be living witnesses of Christ’s love through excellent education.”
The school is also involved in training Nepali teachers and schools to positively impact their local communities through its teacher training program called Education Quality Improvement Programme (EQUIP).


== Accreditation ==
What does Accreditation mean? KISC is an Accredited School
Accreditation encourages and facilitates school improvement because it...

* Requires KISC to establish and implement a yearly improvement plan based on its vision mission and value statements.
* Regularly conducts an examination of the entire school – its community, its philosophy and goals, its programs and services, its facilities, and its financial stability.
* Helps KISC to set priorities for improvement.
* Offers a mechanism for all stakeholders to play a major role in determining KISC's future, helping KISC to be the best it can be.

Accreditation provides a means for public accountability because it ...

Assures KISC's community that the school's purposes are appropriate and are being accomplished through a viable educational program.
Provides an objective, third party evaluation of a school's entire program, services, resources, and results based on professional standards.
Engages KISC in a regular peer review process aimed at ensuring attention to a continuous improvement process.
Validates for the public the integrity of KISC's program and student transcripts.
Accreditation ensures that KISC meets Middle States' internationally recognized professional standards for schools in the areas of....

 *     Philosophy, Mission, Beliefs, and Objectives Planning
 *     Learning Media Services and Technology Finances
 *     Governance and Leadership and Organisational Design of staff
 *     Staff and Student Life and Services
 *     Educational Programs Facilities
 *     Health and Safety
 *     Assessment of Student Learning

Accredited by:
Commission on Secondary Schools of the Middle States Association 3624 Market Place, Philadelphia, PA 19104 www.css-msa.org 


== Primary Curriculum (5-11 yrs) ==
Students are grouped into classrooms for Year K, 1, 2, 3, 4, 5 and 6.
Each class studies the following subjects, designed from the British and International Curriculum: Art, Information & Communication Technology (ICT), Literacy (reading, writing, grammar, spelling), Mathematics, Music, Personal, Social & Health Education (PSHE), Physical Education (PE), Religious Education (RE), Science and World Around Us (Geography, History, Cultural Studies).
KISC Primary seeks to engage student through creative and active learning while affirming all learning styles. We aim to not only develop academically, but value the emotional, physical and spiritual growth of each student.


== Secondary Curriculum ==
11-14 yrs
Students from 11 to 14 years of age are grouped into 3 age level classes of approximately 15 to 20 students and study Art and Design, English, Geography, History, ICT, Mathematics, Music, Nepali Language and Culture, Personal and Social Education, Physical Education, Religious Education, Science and usually one foreign language from a choice of French, German and Spanish. In these subjects, a common curriculum is used, with text materials from the UK, USA, Canada, Australia and other English speaking countries making it as international in content as possible. All courses stress the basic subject skills required in most countries. Specific work on many of the home countries from which our students come is covered in Personal, Health and Social Education (PSHE), as is keeping abreast of what's going on in the world around them.
Sports and other Elective courses are also offered with the selection varying from term to term.
The medium of instruction at KISC is English. However, if teachers are available, students whose native language is not English may have the opportunity to do some studies in their own language. Students whose English is particularly weak may be given individual or small-group Study Support lessons to help them develop their English skills as quickly as possible. 14-16 yrs
Students from 14–18 years are studying along an international education path made up of 2 facets. The first is the American High School Diploma. The second is the International Certificate in Education (ICE) which students work towards in the first 2 years and the AICE Diploma in the second 2 years. The ICE is achieved by studying a selection of subjects most of which students will sit IGCSE exams in at the end of year 11.
Students usually study 8 IGCSE subjects at KISC in Year 10-11 as well as 5 other compulsory non-IGCSE subjects for which they receive credits towards the American High School Diploma.
The International General Certificate of Secondary Education is a two-year course culminating in external assessment by the University of Cambridge Local Examinations Syndicate in the UK. A student may study individual IGCSE subjects or work towards the International Certificate in Education (ICE). The ICE is earned by achieving satisfactory grades in seven IGCSE subjects, which must include at least one subject from each of five groups of subjects, including a language other than English.
Most IGCSE students study eight subjects, five of which are compulsory and three of which are options. All students also attend compulsory non-IGCSE classes in Physical Education, Religious Education, ICT, PSE and Electives. All subjects studied count towards credits required for the USA. Below are the IGCSE Subject options in their appropriate groupings.

Group 1             Group 2         Group 3         Group 4         Group 5
*English Language        *English Literature     *Co-ord. Science (2)    *Mathematics    Business Studies
French   Geography                       Art & Design
German   History                         Music
Spanish                                 

Subjects marked * are compulsory.
Co-ordinated Science, covering Physics, Chemistry and Biology, counts as two subjects at IGCSE.
Future Options Students must study most subjects at IGCSE Level and pass with a Grade C or above in order to take that subject at AS (Advanced Subsidiary Level) in Year 12-13. Students wishing to go to College in the US must take American History and Government (1 credit course) at some point in Year 10-13. Students wishing to take AS English must pass IGCSE English Language and English Literature.
Note Courses for Year 10-11 are two-year courses and it is very difficult for students to catch up on their work if they are absent. KISC therefore recommends that students should not be absent for extended periods of time and that furloughs/Home Leave are avoided where possible or else taken during school holidays. 16-18 yrs
During Term 3 at KISC all Year 11 students who would like to study in Year 12 and 13 are asked to make choices regarding the courses they wish to follow.
AICE Diploma (Equivalent to 3 British A levels or 6 British AS Levels) The Advanced International Certificate of Secondary Education Diploma is a two-year pre-university course. It progresses from IGCSE and culminates in external assessment by the University of Cambridge International Examinations center in the UK. A student may study individual AS (Advance Subsidiary) subjects or work towards the Advanced International Certificate in Education (AICE) Diploma. The AICE Diploma is earned by achieving satisfactory grades in six AS subjects, which must include at least one subject from each of the three groups of subjects
Below are the AS Subject options in their appropriate groupings.

Group 1             Group 2         Group 3
Biology          English         Art and Design
Chemistry        Foreign Language - French       Business Studies
Computing        Foreign Language - German       Geography
Mathematics              History
Physics                 

NB: Geography and Business Studies do not need to be studied at IGCSE level to do the AS level. A Levels are offered in some subjects according to student requirements.
Additional Curriculum All students also have classes in Information and Communication Technology, Physical Education, Religious Education, Personal and Social Education and Electives.


== EQUIIP ==
Kathmandu International Study Centre (KISC) Education Quality Improvement Programme (EQUIP) is a project of Human Development and Community Services (HDCS), a Nepali Non-Governmental Organization.
KISC EQUIP seeks to provide transformational training for school communities that will positive change in all areas of people’s lives, their families, their communities and Nepal.
KISC EQUIP works in partnership with government, community and private schools. KISC EQUIP gives high priority to schools in HDCS project areas to improve the quality of education of those schools to help HDCS to retain and attract Nepali professional staff to work in its projects.


== References ==


== External links ==
Kathmandu International Study Centre Website
KISC on Facebook