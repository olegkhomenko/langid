The International Temperature Scale of 1990 (ITS-90) published by the Consultative Committee for Thermometry (CCT) of the International Committee for Weights and Measures (CIPM) is an equipment calibration standard for making measurements on the Kelvin and Celsius temperature scales. ITS–90 is an approximation of the thermodynamic temperature scale that facilitates the comparability and compatibility of temperature measurements internationally. It specifies fourteen calibration points ranging from 0.65±0 K to 1357.77±0 K (-272.50±0 °C to 1084.62±0 °C) and is subdivided into multiple temperature ranges which overlap in some instances. ITS-90 is the latest (as of 2014) of a series of International Temperature Scales adopted by CIPM since 1927. Adopted at the 1989 General Conference on Weights and Measures, it supersedes the International Practical Temperature Scale of 1968 (amended edition of 1975) and the 1976 "Provisional 0.5 K to 30 K Temperature Scale". CCT has also adopted a mise en pratique (practical instructions) in 2011. The lowest temperature covered by ITS-90 is 0.65 K. In 2000, the temperature scale was extended further, to 0.9 mK, by the adoption of a supplemental scale, known as the Provisional Low Temperature Scale of 2000 (PLTS-2000).


== Details ==
CCT ITS-90 is designed to represent the thermodynamic (absolute) temperature scale (referencing absolute zero) as closely as possible throughout its range. Many different thermometer designs are required to cover the entire range. These include helium vapor pressure thermometers, helium gas thermometers, standard platinum resistance thermometers (known as SPRTs, PRTs or Platinum RTDs) and monochromatic radiation thermometers.
Although the Kelvin and Celsius scales are defined using absolute zero (0 K) and the triple point of water (273.16 K and 0.01 °C), it is impractical to use this definition at temperatures that are very different from the triple point of water. Accordingly, ITS–90 uses numerous defined points, all of which are based on various thermodynamic equilibrium states of fourteen pure chemical elements and one compound (water). Most of the defined points are based on a phase transition; specifically the melting/freezing point of a pure chemical element. However, the deepest cryogenic points are based exclusively on the vapor pressure/temperature relationship of helium and its isotopes whereas the remainder of its cold points (those less than room temperature) are based on triple points. Examples of other defining points are the triple point of hydrogen (−259.3467 °C) and the freezing point of aluminium (660.323 °C).
Thermometers calibrated per ITS–90 use complex mathematical formulas to interpolate between its defined points. ITS–90 specifies rigorous control over variables to ensure reproducibility from lab to lab. For instance, the small effect that atmospheric pressure has upon the various melting points is compensated for (an effect that typically amounts to no more than half a millikelvin across the different altitudes and barometric pressures likely to be encountered). The standard even compensates for the pressure effect due to how deeply the temperature probe is immersed into the sample. ITS–90 also draws a distinction between “freezing” and “melting” points. The distinction depends on whether heat is going into (melting) or out of (freezing) the sample when the measurement is made. Only gallium is measured while melting, all the other metals are measured while the samples are freezing.
A practical effect of ITS–90 is the triple points and the freezing/melting points of its thirteen chemical elements are precisely known for all temperature measurements calibrated per ITS–90 since these thirteen values are fixed by its definition. Only the triple point of Vienna Standard Mean Ocean Water (VSMOW) is known with absolute precision—regardless of the calibration standard employed—because the very definitions of both the Kelvin and Celsius scales are fixed by international agreement based, in part, on this point.


== Limitations ==
There are often small differences between measurements calibrated per ITS–90 and thermodynamic temperature. For instance, precise measurements show that the boiling point of VSMOW water under one standard atmosphere of pressure is actually 373.1339 K (99.9839 °C) when adhering strictly to the two-point definition of thermodynamic temperature. When calibrated to ITS–90, where one must interpolate between the defining points of gallium and indium, the boiling point of VSMOW water is about 10 mK less, about 99.974 °C. The virtue of ITS–90 is that another lab in another part of the world will measure the very same temperature with ease due to the advantages of a comprehensive international calibration standard featuring many conveniently spaced, reproducible, defining points spanning a wide range of temperatures.
Although “International Temperature Scale of 1990” has the word “scale” in its title, this is a misnomer that can be misleading. ITS–90 is not a scale; it is an equipment calibration standard. Temperatures measured with equipment calibrated per ITS–90 may be expressed using any temperature scale such as Celsius, Kelvin, Fahrenheit, or Rankine. For example, a temperature can be measured using equipment calibrated to the kelvin-based ITS–90 standard, and that value may then be converted to, and expressed as, a value on the Fahrenheit scale (e.g. 211.953 °F).
ITS–90 does not address the highly specialized equipment and procedures used for measuring temperatures extremely close to absolute zero. For instance, to measure temperatures in the nanokelvin range (billionths of a kelvin), scientists using optical lattice laser equipment to adiabatically cool atoms, turn off the entrapment lasers and simply measure how far the atoms drift over time to measure their temperature. A cesium atom with a velocity of 7 mm/s is equivalent to a temperature of about 700 nK (which was a record cold temperature achieved by the NIST in 1994).
Estimates of the differences between thermodynamic temperature and the ITS-90 (T-T90) were published in 2010. It had become apparent that ITS-90 deviated considerably from PLTS-2000 in the overlapping range of 0.65 K to 2 K. To address this, a new 3He vapor pressure scale was adopted, known as PTB-2006. For higher temperatures, expected values for T-T90 are below 0.1 mK for temperatures 4.2 K – 8 K, up to 8 mK at temperatures close to 130 K, at 0 K (by definition) at the triple point of water (273.16 °C), but rising again to 10 mK at temperatures close to 430 K, and reaching 46 mK at temperatures close to 1150 K.


== Standard interpolating thermometers and their ranges ==


== Defining points ==
The table below lists the defining fixed points of ITS-90.


== See also ==
Thermodynamic (absolute) temperature — the "true temperature" which ITS-90 is attempting to approximate.
Provisional Low Temperature Scale of 2000 (PLTS-2000) — A newer temperature scale for the range of 0.0009 K to 1 K, based on the melting pressure of helium-3.
Kelvin
Triple point
Vienna Standard Mean Ocean Water (VSMOW)
Resistance thermometer
Platinum resistance thermometer


== References ==

Preston-Thomas H., Metrologia, 1990, 27(1), 3-10 (amended version).
"Mise en pratique for the definition of the kelvin" (PDF). Sèvres, France: Consultative Committee for Thermometry (CCT), International Committee for Weights and Measures (CIPM). 2011. Retrieved 25 June 2013. 
Consultative Committee for Thermometry (CCT) (1989). "The International Temperature Scale of 1990 (ITS-90)" (PDF). Procès-verbaux du Comité International des Poids et Mesures, 78th meeting. Sèvres, France: International Committee for Weights and Measures (CIPM). Retrieved 25 June 2013. 


== External links ==
The Internet ITS-90 Resource (by ISOTech Ltd)
ITS-90 (by Swedish National Testing and Research Institute)
About Temperature Sensors (information repository)
NIST ITS-90 Thermocouple Database (by United States Department of Commerce, National Institute of Standards & Technology)
Conversion among different international temperature scales; equations and algorithms.