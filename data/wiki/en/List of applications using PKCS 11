This article lists applications and other software implementations using the PKCS #11 standard.


== Applications ==
FreeOTFE – disk encryption system (PKCS #11 can either be used to encrypt critical data block, or as keyfile storage)
Mozilla Firefox – a web browser
Mozilla Thunderbird – an email client
OpenDNSSEC – a DNSSEC signer
OpenSSL – TLS/SSL library (with engine_pkcs11)
GnuTLS – TLS/SSL library
OpenVPN – VPN system
StrongSwan – VPN system
TrueCrypt – disk encryption system (PKCS #11 only used as trivial keyfile storage)
TrouSerS – an open-source TCG Software Stack
OpenSC – smartcard library
OpenSSH – a Secure Shell implementation (since OpenSSH version 5.4)
OpenDS – an open source directory server.
PowerDNS – open source, authoritative DNS server (since version 3.4.0)
GNOME Keyring – a password and cryptographic key manager.
Solaris Cryptographic Framework – pluggable cryptographic system in operating system
Safelayer – KeyOne and TrustedX product suites.
Pkcs11Admin – GUI tool for administration of PKCS#11 enabled devices
SoftHSM – implementation of a cryptographic store accessible through a PKCS#11 interface
XCA – X Certificate and key management
SecureCRT – SSH client


== PKCS #11 wrappers ==
Since PKCS #11 is a complex C API many wrappers exist that let the developer use the API from various languages.
NCryptoki - .NET (C# and VB.NET), Silverlight 5 and Visual Basic 6 wrapper for PKCS #11 API
Pkcs11Interop - Open source .NET wrapper for unmanaged PKCS#11 libraries
python-pkcs11 - The most complete and documented PKCS#11 wrapper for Python
PyKCS11 - Another wrapper for Python
pkcs11 - Another wrapper for Python
Java 5.0 includes a wrapper for PKCS #11 API
IAIK PKCS#11 Wrapper - A library for the Java™ platform which makes PKCS#11 modules accessible from within Java.
pkcs11-helper - A simple open source C interface to handle PKCS #11 tokens.
SDeanComponents - Delphi wrapper for PKCS #11 API
jacknji11 - Java wrapper using Java Native Access (JNA)
ruby-pkcs11 - Ruby binding for PKCS #11 API
pkcs11.net - .NET wrapper for PKCS #11 API
Oracle Solaris Cryptographic Framework
pkcs11 - Go wrapper for PKCS #11 API


== References ==