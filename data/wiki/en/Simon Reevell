Simon Justin Reevell (born 2 March 1966) is a British barrister and Conservative Party politician. He was Member of Parliament (MP) for Dewsbury in West Yorkshire during one parliament, losing his seat at the 2015 election.


== Early life ==
Reevell was born and raised in the West Riding of Yorkshire. After attending Manchester Polytechnic and obtaining a degree in Economics, he embarked on a career in the army as an infantry officer, but this was cut short by injury. As a result, he decided to train as a barrister and represented many members of the armed forces before courts-martial. One case, in which Reevell established that members of the TA were sent to Iraq without adequate training, led him to do all that he could to change the incumbent Labour government – which led to him standing to become a Conservative MP for Dewsbury at the 2010 general election.


== Professional career ==
Based in chambers in Leeds, he practises in general criminal law, specialising in defending service personnel at courts-martial both in the UK and abroad.


== Political career ==


=== 2010 general election ===
Reevell was selected to represent the Conservative Party in Dewsbury at the 2010 general election. He won the seat from Labour with a small majority of 1,526 (2.8%) resulting in a 7.8% swing to the Conservatives from Shahid Malik. It was the first time the seat of Dewsbury had been won by a Conservative MP since that of John Whitfield in the 1983 general election, making Reevell only the second Conservative MP since 1922 to win the seat.


=== 2015 general election ===
Reevell was re-selected in 2013 to represent the Conservative Party in Dewsbury at the 2015 general election.
At the 2015 general election Reevell was defeated by Labour's Paula Sherriff who took the seat with a majority of 1,451.


=== 2010-2015 ===
In 2014 it was revealed that Reevell was, after the speaker of the house of commons John Bercow, the most long-winded speaker in parliament, contributing an average of 460 words every time he speaks. Reevell said: “You get a lot of MPs who say things like ‘does my honourable friend agree that tomorrow is Thursday? I don’t do that. When I contribute to a debate it’s an actual speech in which I make serious points."


=== Gaza-Israel conflicts ===
In response to the 2010 Gaza aid envoy crisis Reevell commented that many fellow Tory MP's did not understand the depth of anger amongst Muslim voters regarding the crisis stating; “When the Gaza aid convoy crisis was happening [in 2010], a colleague came to me and commented on how quiet a week it had been,” he said. “Meanwhile, I was getting bombarded with emails about it.”


=== Constituency issues ===


==== Library closures ====
When Kirklees Council announced in 2014 that they were considering closing all of the districts libraries except for Huddersfield and Dewsbury Reevell said; "the situation was a result of under-performance by the council" and that “Any properly run organisation would not be talking about a nuclear option. Any properly run organisation would have planned, evaluated and prioritised. It is short-term thinking. Lots of local authorities are being asked to save a lot of money.” In response the labour leader of Kirklees Council hit back stating that “We wouldn’t be talking about a nuclear option if we didn’t have £140 million cut from our budget every year.” He said that cuts on a national level left councils with no choice but to slash services.


==== Counter-Terrorism and Security Bill dispute ====
In March 2015 Reevell announced he had received a letter from "the Scholars of Dewsbury" complaining that the 2015 Counter-Terrorism and Security Bill “seeks to alienate… the Muslim community” and is for “party political gain”. Reevell hit back stating that such claims were "irresponsible" and "wrong". He went on to say that; “The UK faces an unprecedented threat from people going to Syria to join ISIS and then returning to the UK intending to kill. The purpose of the Counter-Terrorism and Security Bill is to stop this. The Bill may impact most on young British Muslims. Young British Catholics, Anglicans and atheists have not swelled the ranks of ISIS. Those who have left the UK and gloat on social media about jihad purport to be Muslims. But to criticise the Bill for being somehow anti-Muslim is to conflate two separate matters." He also went on to say that; “Your group appears to lack any appreciation of the fact that an absence of credible, relevant leadership within the Muslim community in the UK has contributed significantly to the opportunity for radicalisation amongst this minority of young people." Reevell emphasised to the group that; "Rather than moaning about the potential effects of the Bill, ask what you can do to make the provisions of the Bill unnecessary. If community leaders cannot prevent the flow of those who want to kill, this legislation will do it for them." He also mentioned that; "The Bill was proposed by the Coalition Government and supported by the Labour Party. It has all-party support. At both the Second and Third Readings there was not even a call for a vote." He went on to comment that the groups claims that Muslims do not have the same ‘level of protection in law as other sections of society’ is "wrong". and that; "For it to be advanced on behalf of a group that refers to its members as scholars is beyond disappointing.”


== Personal life ==
He is married to Louise, who is also a barrister. He plays tennis and is interested in late Victorian oil paintings, classic cars and military history. Reevell is a church warden and is an active supporter of the RNLI, Help for Heroes and Dog's Trust.


== References ==