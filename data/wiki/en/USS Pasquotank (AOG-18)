USS Pasquotank (AOG-18) was a Mettawee-class T1 tanker type gasoline tanker acquired by the U.S. Navy for the dangerous task of transporting gasoline to warships in the fleet, and to remote Navy stations.
Pasquotank was laid down as SS Tongue River, MC hull 900, on 13 August 1942 by East Coast Shipyard Inc., Bayonne, New Jersey; launched 28 November 1942; originally classified as YOG–48, she was reclassified AOG–18, 25 March 1943 and named Pasquotank the same day; acquired by the Navy 15 April 1943; and commissioned 26 August 1943, Lt. Alford R. Norris USNR in command.


== World War II service ==
After shakedown, Pasquotank sailed from New York Harbor, in convoy, for Aruba, Netherlands West Indies, where she loaded fuel oil and proceeded, via the Panama Canal, to Bora Bora, Society Islands, in the South Pacific Ocean. She arrived Bora Bora, 3 December 1943, and from there, through February 1944, she traveled extensively in the Fiji, New Hebrides, and Solomon Islands, carrying aviation gas and diesel fuel. She unloaded fuel for U.S. Marine air groups at perimeter strips in the Torokina area during March, while they were under siege by the Japanese. In May she set up a shuttle service for the Air Force which was conducting raids against Truk, Rabaul, and Kavieng from Green and Treasury Islands.


=== South Pacific operations ===
Through July she operated in the New Guinea area, and in August began a seven month stint as station tanker at Seeadler Harbor in the Admiralties, servicing escort carriers and cruisers. Pasquotank was detached from this assignment 20 March 1945 and sailed for Leyte.
At Leyte, she operated from San Pedro Bay into August, servicing small craft, and was at Manus on V–J Day. During September she fueled transports in Hollandia Bay, and returned to San Pedro Bay for shuttle service into November.


== Post-war decommissioning ==
She then returned to San Francisco, California, in late December, and decommissioned 27 March 1946. Pasquotank was struck from the Naval Vessel Register 21 May 1946, transferred to the Maritime Commission 1 July 1946 and served as SS Tongue River until scrapped in 1964.


== References ==
This article incorporates text from the public domain Dictionary of American Naval Fighting Ships.


== External links ==
NavSource Online: Service Ship Photo Archive - YOG-48 / AOG-18 Pasquotank